package utils;

import org.junit.Test;
import static org.junit.Assert.*;

public class InputInspectorTest {

    private InputInspector inputInspector = new InputInspector();

    @Test
    public void testIsWebsite() {
        System.out.println(1402104596^543244546);
    }

    @Test
    public void testurl() {

        assertTrue(inputInspector.isUrl("http://www.yandex.com"));
        assertTrue(inputInspector.isUrl("www.mail.ru"));
        assertTrue(inputInspector.isUrl("orel.yandex.ru"));
        assertTrue(inputInspector.isUrl("yandex.com"));
        assertTrue(inputInspector.isUrl("www.yandex.com"));
        assertFalse(inputInspector.isUrl("igl.igl543217@mail.vo"));
        assertFalse(inputInspector.isUrl("ri@ru.ru"));
        assertFalse(inputInspector.isUrl("ru"));
        assertFalse(inputInspector.isUrl(""));
        assertFalse(inputInspector.isUrl("sdfhgjh//u"));
        assertFalse(inputInspector.isUrl("http:ewr.rod"));
        assertFalse(inputInspector.isUrl("http//ynads.cs"));
        assertFalse(inputInspector.isUrl("http://ynads"));
        assertTrue(inputInspector.isUrl("http://www.vk.ru"));
    }

    @Test
    public void testIsPassword() {


    }

    @Test
    public void testIsFileName() {
        assertTrue(inputInspector.isPostcode("00000000"));
        assertTrue(inputInspector.isPostcode("sdfwr24"));
        assertTrue(inputInspector.isPostcode("hg432"));
        assertTrue(inputInspector.isPostcode("112342"));
        assertTrue(inputInspector.isPostcode("feg0000"));
        assertTrue(inputInspector.isPostcode("55555"));
        assertTrue(inputInspector.isPostcode("666666"));
        assertTrue(inputInspector.isPostcode("7777777"));
        assertTrue(inputInspector.isPostcode("88888888"));
        assertTrue(inputInspector.isPostcode("afwe3g44"));
        assertTrue(inputInspector.isPostcode("000300"));
        assertFalse(inputInspector.isPostcode(null));
        assertFalse(inputInspector.isPostcode(""));
        assertFalse(inputInspector.isPostcode("1"));
        assertFalse(inputInspector.isPostcode("22"));
        assertFalse(inputInspector.isPostcode("333"));
        assertFalse(inputInspector.isPostcode("3231/"));
        assertFalse(inputInspector.isPostcode("aaaa\\"));
        assertFalse(inputInspector.isPostcode("23543\n"));
        assertFalse(inputInspector.isPostcode("gferffderer"));
        assertFalse(inputInspector.isPostcode("2345547474"));
        assertFalse(inputInspector.isPostcode("3423 2d"));
    }

    @Test
    public void testIsSomeName() {
        assertFalse(inputInspector.isSimpleName("Lbvf1"));
        assertFalse(inputInspector.isSimpleName("Lb_vf"));
        assertFalse(inputInspector.isSimpleName("Lbv f"));
        assertFalse(inputInspector.isSimpleName(""));
        assertFalse(inputInspector.isSimpleName(".Lbvf"));
        assertFalse(inputInspector.isSimpleName(null));
        assertFalse(inputInspector.isSimpleName("LbvrtytfsdghfsDSAGFHGagshdgjfhkgdfhtrf"));
        assertTrue(inputInspector.isSimpleName("Lbvf"));
        assertTrue(inputInspector.isSimpleName("Dima"));
        assertTrue(inputInspector.isSimpleName("d"));
        assertTrue(inputInspector.isSimpleName("LbdыаваКвёf"));
        assertTrue(inputInspector.isSimpleName("тимвап"));
        assertTrue(inputInspector.isSimpleName("wetfwe"));
        assertTrue(inputInspector.isSimpleName("retdgbfegwd"));
    }

    @Test
    public void testIsId() {

    }

    @Test
    public void testIsPostcode() {

    }
}