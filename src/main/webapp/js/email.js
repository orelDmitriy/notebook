function send() {
    if (hasIncorrectFieldsInEmailPage()) {
        return;
    }

	var form = getFormWithCommand('SendEmail');
    addAllParametersFromForm(form, 'email');
    addParameterToForm(form, 'idRecipients', document.getElementById('idRecipients').value);
	submit(form);
}

function cancel() {
	var form = getFormWithCommand('GetContacts'); 
	submit(form);
}

function saveTemplate(){
    if (hasIncorrectFieldsInEmailPage()) {
        return;
    }
    var subject = document.getElementById("subject");
    var form = getFormWithCommand('SaveTemplate');
    addAllParametersFromForm(form, 'email');
    addParameterToForm(form, 'idRecipients', document.getElementById('idRecipients').value);
    submit(form);
}

function deleteTemplate(){
    var form = getFormWithCommand('DeleteTemplate');
    addAllParametersFromForm(form, 'email');
    addParameterToForm(form, 'idRecipients', document.getElementById('idRecipients').value);
    submit(form);
}

function hasIncorrectFieldsInEmailPage() {
    if(isIncorrectField("subject")){
        return true;
    }
    if(isIncorrectField("message")){
        return true;
    }
    return false;

    function isIncorrectField(idField){
        var field = document.getElementById(idField);
        var regExp = /[a-zа-я0-9]+/i;
        if (field.value != "") {
            if (regExp.test(field.value)) {
                return false;
            }
        }
        field.classList.add('inputError');
        return true;
    }
}

function clearInputEmailElement(element) {
    if (element.classList.contains('inputError')) {
        element.classList.remove('inputError');
    }
    if (element.classList.contains('inputError')){
        element.classList.remove('inputError');
    }
}

function setTemplate(select){
    var subject = document.getElementById("subject");
    subject.value = select.value;
    var messageValue = document.getElementById("message" + select.value);
    var message = document.getElementById("message");
    message.value = messageValue.value;
}