function deleteAttachments() {
	var attachmentTable = document.getElementById("attachments");
    for (var i = 1; i < attachmentTable.rows.length; i++) {
		var row = attachmentTable.rows[i];
        if (row.id > 0 && row.cells[0].childNodes[0].checked) {
			deleteInputFile();
			attachmentTable.deleteRow(i);
			if (attachmentTable.rows.length < 8) {
				insertLastRow();
			}
			i--;
		}
	}
    attachmentTable.lastSelected = -1;
    return;

    function deleteInputFile() {
        var inputFile = document.getElementById(row.id + "input");
        if (inputFile != null) {
            inputFile.parentNode.removeChild(inputFile);
        }
    }

    function insertLastRow() {
        attachmentTable.insertRow();
        var tr = attachmentTable.rows[attachmentTable.rows.length - 1];
        tr.id = -1;
        tr.innerHTML = "<td class='check'></td><td class='fileName'></td><td class='date'></td><td class='comment'></td>";
    }
}

function showAttachmentPopup() {
	if (document.getElementById('attachmentEdit') == null) {
		new AttachmentPopup(-1);
	}
}

function editAttachmentPopup() {
	if (document.getElementById('attachmentEdit') == null) {
		var attachmentTable = document.getElementById("attachments");
		var currentRow = attachmentTable.lastSelected;
		if (currentRow != null && currentRow > 0) {
			new AttachmentPopup(attachmentTable.rows[currentRow].id);
		} else {
			new AttachmentPopup(-1);
		}
	}
}

function addAttachmentsFromTable(form) {
    var table = document.getElementById("attachments");
    var attachmentsIds = "a";
    for (var i = 1; i < table.rows.length; i++) {
        var row = table.rows[i];
        if (row.id > 0) {
            addParameter(1, "name");
            addParameter(2, "date");
            addParameter(3, "comment");
            attachmentsIds = attachmentsIds.concat("," + row.id);
        }
    }
    addParameterToForm(form, "attachmentsIds", attachmentsIds);
    return;

    function addParameter(cell, name) {
        var parameterName = row.id + name;
        var parameterValue = "";
        if ( row.cells[cell].childNodes[0].childNodes[0] != undefined) {
            parameterValue = row.cells[cell].childNodes[0].childNodes[0].wholeText;
        }
        addParameterToForm(form, parameterName, parameterValue);
    }

}

function AttachmentPopup(selectedId) {
    var mainDiv = createMain();
    addCloseButton();
    addTitle();
    var formAttachment = addForm();
    var inputFile = addInputFile();
    var fileName = null;
    var inputComment = addInputComment();
    var buttonGroupDiv = addButtonGroup();
    addButton(addAttachment, 'Добавить', 'btn-success');
    addButton(closeAttachment, 'Назад', 'btn-warning');
	createOverlay();

    if (selectedId > 0) {
       fillFields();
    }
    return;

    function createMain() {
        var mainDiv = document.createElement('div');
        mainDiv.id = 'attachmentEdit';
        mainDiv.classList.add('popup');
        mainDiv.style.display = 'block';
        document.body.appendChild(mainDiv);
        return mainDiv;
    }

    function addForm() {
        var formAttachment = document.createElement("form");
        mainDiv.appendChild(formAttachment);
        return formAttachment;
    }

    function addCloseButton() {
        var buttonClose = document.createElement('a');
        buttonClose.href = '#attachmentsCheck';
        buttonClose.onclick = closeAttachment;
        mainDiv.appendChild(buttonClose);
    }

    function addTitle() {
        var title = document.createElement('h4');
        title.appendChild(document.createTextNode('Редактировать'));
        mainDiv.appendChild(title);
    }

    function addInputFile() {
        var inputFile = document.getElementById(selectedId + "input");
        var fileDiv = getDivFormGroup();
        formAttachment.appendChild(fileDiv);
        if (inputFile == null) {
            inputFile = document.createElement('input');
            inputFile.type = 'file';
            inputFile.isChange = false;
            inputFile.onchange = function () {
                inputFile.isChange = true;
                inputFile.classList.remove('inputError');
            }
        }
        fileDiv.appendChild(inputFile);
        return inputFile;
    }

    function addInputComment() {
        var inputComment = document.createElement('textarea');
        var commentDiv = getDivFormGroup();
        inputComment.id = 'comment';
        inputComment.placeholder = 'Комментарий';
        inputComment.rows = 3;
        inputComment.cols = 36;
        inputComment.maxLength = 255;
        inputComment.autocomplete = "off";

        commentDiv.appendChild(inputComment);
        formAttachment.appendChild(commentDiv);
        return inputComment;
    }

    function addButtonGroup() {
        var buttonGroupDiv = document.createElement('div');
        buttonGroupDiv.classList.add('btn-group', 'full');
        mainDiv.appendChild(buttonGroupDiv);
        return buttonGroupDiv;
    }

    function addButton(func, name, style) {
        var buttonAdd = document.createElement('button');
        buttonAdd.classList.add(style, 'btn');
        buttonAdd.onclick = func;
        buttonAdd.appendChild(document.createTextNode(name));
        buttonGroupDiv.appendChild(buttonAdd);
    }

    function fillFields() {
        var row = document.getElementById(selectedId);
        if (row.cells[3].childNodes[0].childNodes[0] != undefined) {
            inputComment.value = row.cells[3].childNodes[0].childNodes[0].wholeText;
        }
    }

	function closeAttachment() {
		document.body.removeChild(document.getElementById('attachmentEdit'));
		document.body.removeChild(document.getElementById('overlay'));
	}

	function addAttachment() {
        if (hasIncorrectFields()) {
            return;
        }
		var attachmentTable = document.getElementById('attachments');
        var position = getPosition();
        var row = rewriteRow();
        row.id = getAttachmentRowId();
        addCheckBox();
        addFileName();
        addDate();
        addComment();
        addInputFile();
		closeAttachment();

        function hasIncorrectFields() {
            inputFile.classList.remove('inputError');
            if (!inputFile.isChange && selectedId == -1) {
                inputFile.classList.add('inputError');
                return true;
            }
            return false;
        }

        function getPosition() {
            var position;
            if (selectedId > 0) {
                position = document.getElementById("attachments").lastSelected;
                document.getElementById("attachments").lastSelected = -1;
            } else {
                position = getFirstEmptyRow(attachmentTable);
            }
            return position;
        }

        function rewriteRow() {
            if (position < attachmentTable.rows.length) {
                var oldRow = attachmentTable.rows[position];
                attachmentTable.deleteRow(position);
                fileName = oldRow.cells[1].innerHTML;
            }
            var row = attachmentTable.insertRow(position);
            attachmentTable.lastSelected = -1;
            return row;
        }

        function getAttachmentRowId() {
            if (selectedId > 0) {
                return selectedId;
            } else {
                return getUniqueIdForAttachment(100);
            }
        }

        function getUniqueIdForAttachment(min) {
            var id = min;
            if (document.getElementById(id) == null) {
                return id;
            }
            return getUniqueIdForAttachment(++min);
        }

        function addCheckBox() {
            var td = row.insertCell(0);
            td.classList.add('check');
            var inputCheck = document.createElement('input');
            inputCheck.type = 'checkbox';
            td.appendChild(inputCheck);
        }

        function addFileName() {
            var td = row.insertCell(1);
            td.classList.add('fileName');
            if (inputFile.isChange) {
                td.innerHTML = "<div>" + inputFile.files[0].name + "</div>";
            } else {
                td.innerHTML = "<div>" + fileName + "</div>";
            }
        }

        function addDate() {
            var td = row.insertCell(2);
            td.classList.add('date');
            td.innerHTML = "<div>" + formatDate(new Date()) + "</div>";
        }

        function addComment() {
            var td = row.insertCell(3);
            td.classList.add('comment');
            td.innerHTML = "<div>" + inputComment.value + "</div>";
        }

        function addInputFile() {
            var formFiles = document.getElementById('formFiles');
            inputFile.id = row.id + "input";
            inputFile.name = row.id + "input";
            formFiles.appendChild(inputFile);
        }

	}

	function formatDate(date) {
		var mm = date.getMonth() + 1;
		var dd = date.getDate();

		return [ date.getFullYear(), '-', String(mm).length == 1 ? '0' : '',
				mm, '-', String(dd).length == 1 ? '0' : '', dd ].join('');
	}
}