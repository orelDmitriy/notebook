function cancel() {
	var form = getFormWithCommand('ToIndex');
	submit(form);
}

function registration() {
    if (isNotEmail('email')){
        return;
    }
    if (!isCorrectPasswords()){
        return;
    }

    var form = getFormWithCommand('Registration');
    addAllParametersFromForm(form, 'registration');
    submit(form);

    function isCorrectPasswords(){
        var password = document.getElementById("password");
        var repeat = document.getElementById("repeat");
        if (password.value.length < 6) {
            password.type = "input";
            password.value = "Пароль слишком короткий.";
            password.classList.add('inputError');
            return false;
        }
        if (password.value != repeat.value) {
            repeat.type = "input";
            repeat.value = "Несовпадение паролей.";
            repeat.classList.add('inputError');
            return false;
        }
        return true;
    }
}