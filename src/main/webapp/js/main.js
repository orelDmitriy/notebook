function newContact() {
	var form = getFormWithCommand('ToEditContact');
	submit(form);
}

function editContact(idContact) {
	var form = getFormWithCommand('ToEditContact');
	var table = document.getElementById('tableContacts');
    if (idContact == undefined && table.lastSelected !== undefined){
        idContact = table.rows[table.lastSelected].id;
    }
    addParameterToForm(form, 'idContact', idContact);
	submit(form);
}

function deleteContact() {
	var form = getFormWithCommand('DeleteContact');
    var deleteIds = getCheckedIds();
    addParameterToForm(form, 'deleteIds', deleteIds);
	submit(form);
}

function getCheckedIds() {
    var tableContacts = document.getElementById("tableContacts");
    var ids = "";
    for (var i = 1; i < tableContacts.rows.length; i++) {
        var row = tableContacts.rows[i];
        if (row.id > 0 && row.cells[0].childNodes[0].checked) {
            if (ids.length > 0){
                ids += ",";
            }
            ids += row.id;
        }
    }
    return ids;
}

function forwardEmail() {
	var form = getFormWithCommand('ToEmail');
    var idRecipients = getCheckedIds();
    addParameterToForm(form, 'idRecipients', idRecipients);
	submit(form);
}

function searchContacts() {
	var form = getFormWithCommand('ToSearchContacts');
	submit(form);
}

function logout() {
	var form = getFormWithCommand('Logout');
	submit(form);
}

function swapLimit(amount) {
	var form = getFormWithCommand('GetContacts');
	addParameterToForm(form, 'limitForPage', amount);
	submit(form);
}

function gotoPage(number){
    var form = getFormWithCommand('GetContacts');
    addParameterToForm(form, 'numberPage', number);
    submit(form);
}

function clearFilter(){
    var form = getFormWithCommand('GetContactsWithParam');
    submit(form);
}

