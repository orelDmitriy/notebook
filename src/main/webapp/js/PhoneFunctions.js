function deletePhones() {
    var phonesTable = document.getElementById("phones");
    for (var i = 1; i < phonesTable.rows.length; i++) {
        var row = phonesTable.rows[i];
        if (getClearIdPhone(row) > 0 && row.cells[0].childNodes[0].checked) {
            phonesTable.deleteRow(i);
            if (phonesTable.rows.length < 8) {
                insertLastRow();
            }
            i--;
        }
    }
    phonesTable.lastSelected = -1;

    function insertLastRow() {
        phonesTable.insertRow();
        var tr = phonesTable.rows[phonesTable.rows.length - 1];
        tr.id = -1;
        tr.innerHTML = "<td class='check'></td><td class='phoneNumber'></td><td class='description'></td><td class='comment'></td>";
    }
}

function showPopupPhone() {
    if (document.getElementById('phoneEdit') == null) {
        new PhonePopup(-1);
    }
}

function editPhonePopup() {
    if (document.getElementById('phoneEdit') == null) {
        var phoneTable = document.getElementById("phones");
        var currentRow = phoneTable.lastSelected;
        if (currentRow != null && currentRow > 0) {
            new PhonePopup(getClearIdPhone(phoneTable.rows[currentRow]));
        } else {
            new PhonePopup(-1);
        }
    }
}

function getClearIdPhone(row) {
    return row.id.substr(0, row.id.length - 1);
}

function addPhonesFromTable(form) {
    var table = document.getElementById("phones");
    var phonesIds = "p";
    for (var i = 1; i < table.rows.length; i++) {
        var row = table.rows[i];
        if (getClearIdPhone(row, i) > 0) {
            addParameter(1, "number");
            addParameter(2, "type");
            addParameter(3, "comment");
            phonesIds = phonesIds.concat("," + row.id);
        }
    }
    addParameterToForm(form, "phonesIds", phonesIds);

    function addParameter(cell, name) {
        var parameterName = row.id + name;
        var parameterValue = "";
        if ( row.cells[cell].childNodes[0].childNodes[0] != undefined) {
            parameterValue = row.cells[cell].childNodes[0].childNodes[0].wholeText;
        }
        addParameterToForm(form, parameterName, parameterValue);
    }
}

function getCountryCode(fullNumber) {
    return fullNumber.substr(1, fullNumber.indexOf('(') - 1);
}

function getOperatorCode(fullNumber) {
    return fullNumber.substr(fullNumber.indexOf('(') + 1, fullNumber.indexOf(')') - 1 - fullNumber.indexOf('('));
}

function getNumber(fullNumber) {
    return fullNumber.substr(fullNumber.indexOf(')') + 1);
}

function PhonePopup(selectedId) {
    var mainDiv = createMainDiv();
    addCloseButton();
    addTitle();
    var formPhone = addForm();
    var inputCountryCode = addInputFiled("countryCode", "Код страны 375", 4);
    var inputOperatorCode = addInputFiled("operatorCode", "Код оператора 29", 5);
    var inputNumber = addInputFiled("number", "Номер 7677764", 7);
    var selectType = addSelectType();
    var inputComment = addInputComment();
    var buttonGroup = addButtonGroup();
    addButton(addPhone, 'Сохранить', 'btn-success');
    addButton(closePhone, 'Назад', 'btn-warning');
    if (selectedId > 0) {
        fillFields();
    }
    var overlay = createOverlay();

    function createMainDiv() {
        var mainDiv = document.createElement('div');
        mainDiv.id = 'phoneEdit';
        mainDiv.classList.add('popup', 'popupPhoneDiv');
        mainDiv.style.display = 'block';
        document.body.appendChild(mainDiv);
        return mainDiv;
    }

    function addForm() {
        var formPhone = document.createElement("form");
        formPhone.classList.add('phonePopupForm');
        mainDiv.appendChild(formPhone);
        return formPhone;
    }

    function addCloseButton() {
        var buttonClose = document.createElement('a');
        buttonClose.href = '#phoneCheck';
        buttonClose.onclick = closePhone;
        mainDiv.appendChild(buttonClose);
    }

    function addTitle() {
        var title = document.createElement('h4');
        title.appendChild(document.createTextNode('Редактировать'));
        mainDiv.appendChild(title);
    }

    function addInputFiled(fieldName, placeHolder, maxLength) {
        var blockDiv = getDivFormGroup();
        var inputField = document.createElement('input');
        inputField.type = 'text';
        inputField.name = fieldName;
        inputField.placeholder = placeHolder;
        inputField.maxLength = maxLength;
        inputField.autocomplete = "off";
        inputField.onfocus = function () {
            clearInputElement(this);
        };
        blockDiv.appendChild(inputField);
        formPhone.appendChild(blockDiv);
        return inputField;
    }

    function addSelectType() {
        var selectType = document.createElement("select");
        var typeDiv = getDivFormGroup();
        selectType.name = 'type';
        selectType.onchange = function () {
            if (selectType.value == 'домашний' && inputNumber.value.length > 6) {
                inputNumber.value = inputNumber.value.substr(0, 6);
                inputNumber.maxLength = 6;
            } else {
                inputNumber.maxLength = 7;
            }
        };
        var optionMobile = document.createElement("option");
        optionMobile.innerHTML = 'мобильный';
        selectType.appendChild(optionMobile);
        var optionHome = document.createElement("option");
        optionHome.innerHTML = 'домашний';
        selectType.appendChild(optionHome);
        typeDiv.appendChild(selectType);
        formPhone.appendChild(typeDiv);
        return selectType;
    }

    function addInputComment() {
        var inputComment = document.createElement('textarea');
        var commentDiv = getDivFormGroup();
        inputComment.id = 'comment';
        inputComment.placeholder = 'Комментарий';
        inputComment.rows = 3;
        inputComment.cols = 22;
        inputComment.maxLength = 255;
        inputComment.autocomplete = "off";
        commentDiv.appendChild(inputComment);
        formPhone.appendChild(commentDiv);
        return inputComment;
    }

    function addButtonGroup() {
        var buttonGroup = document.createElement('div');
        buttonGroup.classList.add('btn-group', 'full');
        mainDiv.appendChild(buttonGroup);
        return buttonGroup;
    }

    function addButton(func, text, style) {
        var button = document.createElement('button');
        button.classList.add(style, 'btn');
        button.onclick = func;
        button.appendChild(document.createTextNode(text));
        buttonGroup.appendChild(button);
    }

    function fillFields() {
        var data = document.getElementById(selectedId + "p");
        var fullNumber = data.cells[1].childNodes[0].childNodes[0].wholeText;
        inputCountryCode.value = getCountryCode(fullNumber);
        inputOperatorCode.value = getOperatorCode(fullNumber);
        inputNumber.value = getNumber(fullNumber);
        var type = data.cells[2].childNodes[0].childNodes[0].wholeText;
        if (type == 'домашний') {
            selectType.childNodes[1].selected = 'selected';
            inputNumber.maxLength = 6;
        }
        if (data.cells[3].childNodes[0].childNodes[0] != undefined) {
            inputComment.value = data.cells[3].childNodes[0].childNodes[0].wholeText;
        }

    }

    function closePhone() {
        document.body.removeChild(document.getElementById('phoneEdit'));
        document.body.removeChild(document.getElementById('overlay'));
    }

    function addPhone() {
        if (hasIncorrectFields()) {
            return;
        }
        var phoneTable = document.getElementById('phones');
        var position = getInsertPosition();
        var row = rewriteRow();
        row.id = getPhoneRowId();
        addCheckBox();
        addPhoneNumber();
        addDescription();
        addComment();
        closePhone();

        function hasIncorrectFields() {
            inputCountryCode.classList.remove('inputError');
            inputOperatorCode.classList.remove('inputError');
            inputNumber.classList.remove('inputError');
            var regExp = /^[0-9]{1,4}$/;
            var result = false;
            if (!regExp.test(inputCountryCode.value)) {
                inputCountryCode.tmpValue = inputCountryCode.value;
                inputCountryCode.classList.add('inputError');
                inputCountryCode.value = 'Неверный код страны.';
                result = true;
            }
            regExp = /^[0-9]{2,5}$/;
            if (!regExp.test(inputOperatorCode.value)) {
                inputOperatorCode.tmpValue = inputOperatorCode.value;
                inputOperatorCode.classList.add('inputError');
                inputOperatorCode.value = 'Неверный код оператора.';
                result = true;
            }
            regExp = /^[0-9]{7}$/;
            if (selectType.value == 'домашний') {
                regExp = /^[0-9]{6}$/;
            }
            if (!regExp.test(inputNumber.value)) {
                inputNumber.tmpValue = inputNumber.value;
                inputNumber.value = 'Неверный номер.';
                inputNumber.classList.add('inputError');
                result = true;
            }
            return result;
        }

        function getInsertPosition() {
            var result;
            if (selectedId > 0) {
                result = phoneTable.lastSelected;
                phoneTable.lastSelected = -1;
            } else {
                result = getFirstEmptyRow(phoneTable);
            }
            return result;
        }

        function rewriteRow() {
            if (position < phoneTable.rows.length) {
                phoneTable.deleteRow(position);
            }
            return phoneTable.insertRow(position);
        }

        function getPhoneRowId() {
            var result;
            if (selectedId > 0) {
                result = selectedId;
            } else {
                result = getUniqueIdForPhone(100);
            }
            return result + "p";
        }

        function getUniqueIdForPhone(min) {
            var id = min;
            if (document.getElementById(id + "p") == null) {
                return id;
            }
            return getUniqueIdForPhone(++min);
        }

        function addCheckBox() {
            var td = row.insertCell(0);
            td.classList.add('check');
            var inputCheck = document.createElement('input');
            inputCheck.type = 'checkbox';
            td.appendChild(inputCheck);
        }

        function addPhoneNumber() {
            var td = row.insertCell(1);
            td.classList.add('phoneNumber');
            td.innerHTML = "<div>+" + inputCountryCode.value + "(" + inputOperatorCode.value + ")" + inputNumber.value + "</div>";
        }

        function addDescription() {
            var td = row.insertCell(2);
            td.classList.add('description');
            td.innerHTML = "<div>" + selectType.value + "</div>";
        }

        function addComment() {
            var td = row.insertCell(3);
            td.classList.add('comment');
            td.innerHTML = '<div>' + inputComment.value + '</div>';
        }
    }
}
