function search() {
    if (hasIncorrectFields()) {
        return;
    }
    if(incorrectDate(document.getElementById("minAge"))){
       return;
    }
    if(incorrectDate(document.getElementById("maxAge"))){
        return;
    }
	var form = getFormWithCommand('GetContactsWithParam');
    addAllParametersFromForm(form, 'leftForm');
    addAllParametersFromForm(form, 'rightForm');
	submit(form);
}

function cancel() {
	var form = getFormWithCommand('GetContacts'); 
	submit(form);
}

function setMinAge(){
    var minAge = document.getElementById('minAge');
    var maxAge = document.getElementById('maxAge');
    maxAge.min = minAge.value;
}

function setMaxAge(){
    var minAge = document.getElementById('minAge');
    var maxAge = document.getElementById('maxAge');
    minAge.max = maxAge.value;
}