function login() {
    if (isNotEmail('email')){
        return;
    }
    if (shortPassword()){
        return;
    }
	var form = getFormWithCommand('Login'); 
	addAllParametersFromForm(form, 'login');
	submit(form);

    function shortPassword(){
        var password = document.getElementById("password");
        if (password.value.length < 6) {
            password.type = "input";
            password.value = "Пароль слишком короткий.";
            password.classList.add('inputError');
            return true;
        }
        return false;
    }
}

function toRegistration() {
	var form = getFormWithCommand('ToRegistration'); 
	submit(form);
}