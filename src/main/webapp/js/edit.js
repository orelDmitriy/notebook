addEvent(window, "load", chooseLabelForStatus);

function chooseLabelForStatus() {
    var sex = document.getElementById("sex");
    var singleMan = document.getElementById("singleMan");
    var marriedMan = document.getElementById("marriedMan");
    var singleWoman = document.getElementById("singleWoman");
    var marriedWoman = document.getElementById("marriedWoman");

    if (sex.value == "женский") {
        setOptionsForWoman();
    } else {
        setOptionsForMan();
    }
    return;

    function setOptionsForWoman() {
        singleMan.classList.add("hidden");
        marriedMan.classList.add("hidden");
        singleWoman.classList.remove("hidden");
        marriedWoman.classList.remove("hidden");
        if (document.getElementById("maritalStatus").value == "не замужем"
            || document.getElementById("maritalStatus").value == "холост") {
            singleWoman.selected = "selected";
        } else {
            marriedWoman.selected = "selected";
        }
    }

    function setOptionsForMan() {
        singleMan.classList.remove("hidden");
        marriedMan.classList.remove("hidden");
        singleWoman.classList.add("hidden");
        marriedWoman.classList.add("hidden");
        if (document.getElementById("maritalStatus").value == "не замужем"
            || document.getElementById("maritalStatus").value == "холост") {
            singleMan.selected = "selected";
        } else {
            marriedMan.selected = "selected";
        }
    }
}

function saveContact() {
    if (hasIncorrectFieldsInEditPage()) {
        return;
    }
    var form = getFormWithCommand('SaveContact');
    addAllParametersFromForm(form, 'edit');
    addFiles(form);
    addAttachmentsFromTable(form);
    addPhonesFromTable(form);
    submit(form);

    function hasIncorrectFieldsInEditPage() {
        var result = hasIncorrectFields();
        checkUrl();
        checkNewEmail();
        checkName();
        if (incorrectDate(document.getElementById("birthday"))) {
            result = true;
        }
        return result;

        function checkUrl() {
            var regExp = /(http:\/\/|https:\/\/)?(www\u002E)?([a-z0-9]+)(\u002E[a-z0-9]+)*\u002E[a-z]{2,4}/i;
            var inputUrl = document.getElementById("webSite");
            if (inputUrl.value != "") {
                if (!regExp.test(inputUrl.value)) {
                    inputUrl.classList.add('inputError');
                    inputUrl.tmpValue = inputField.value;
                    inputUrl.value = 'Пример: www.mysite.ru';
                    result = true;
                }
            }
        }

        function checkNewEmail() {
            var regExp = /^[a-z\d]+[a-z\d\u002E\u005F]*[a-z\d]+@([a-z]){2,10}\u002E[a-z]{2,4}$/i;
            var email = document.getElementById("email");
            if (email.value != "") {
                if (!regExp.test(email.value)) {
                    email.classList.add('inputError');
                    email.tmpValue = inputField.value;
                    email.value = 'Пример: email@mail.ru';
                    result = true;
                }
            }
        }

        function checkName() {
            var name = document.getElementById("name");
            if (name.value.length < 3) {
                name.classList.add('inputError');
                name.tmpValue = name.value;
                name.value = 'Не меньше 3 букв.';
                result = true;
            }
        }
    }
}

function cancel() {
    var form = getFormWithCommand('GetContacts');
    submit(form);
}

function addFiles(form) {
    var formFiles = document.getElementById('formFiles');
    for (var i = 0; i < formFiles.elements.length;) {
        form.appendChild(formFiles.elements[i]);
    }
}

function choosePhoto() {
    document.getElementById('loadPhoto').addEventListener('change',
        previewPhoto, false);
    document.getElementById('loadPhoto').click();
}

function previewPhoto() {
    var photo = document.getElementById('photo');
    var file = document.getElementById('loadPhoto').files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        photo.src = reader.result;
    };

    if (file) {
        reader.readAsDataURL(file);
    }
}

function getFirstEmptyRow(table) {
    for (var i = 1; i < table.rows.length; i++) {
        if (table.rows[i].id < 0) {
            return i;
        }
    }
    return table.rows.length;
}

function createOverlay() {
    var overlay = document.createElement('div');
    overlay.id = 'overlay';
    overlay.style.filter = 'alpha(opacity=80)';
    overlay.style.display = 'block';
    document.body.appendChild(overlay);
    return overlay;
}
