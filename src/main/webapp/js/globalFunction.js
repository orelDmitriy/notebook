window.onload=function(){
    if (haveMessages()) {
        InfoPopup();
    }
    replaceInputDate();

    function replaceInputDate() {
        var inputs = document.getElementsByTagName("input");
        for(var i = 0; i < inputs.length; i++){
            if (inputs[i].type == "date"){
                inputs[i].type = "input";
                inputs[i].placeholder = "1991.02.25";
                inputs[i].onkeypress = function(e){
                    var keyChar = String.fromCharCode(e.keyCode || e.charCode);
                    var regExp = /^[0-9\.\/\-,]$/;
                    if (regExp.test(keyChar) && this.value.length < 11){
                        return true;
                    }
                    return false;
                }
            }
        }
    }
}

function hasIncorrectFields() {
    var result = false;

    checkFieldsLikeName('name');
    checkFieldsLikeName('surname');
    checkFieldsLikeName('patronymic');
    checkFieldsLikeName('nationality');
    checkFieldsLikeName('country');
    checkFieldsLikeName('city');
    checkFieldsLikeName('street');
    checkInt("home");
    checkInt("room");

    var regExp = /^([0-9A-Z]{5,8})?$/i;
    var inputField = document.getElementById('postcode');
    if (!regExp.test(inputField.value)) {
        inputField.classList.add('inputError');
        inputField.value = 'От 5 до 8 букв и цифр.';
        result = true;
    }

    return result;

    function checkInt(fieldId){
        regExp = /^[0-9]{0,5}?$/;
        inputField = document.getElementById(fieldId);
        if (!regExp.test(inputField.value)) {
            inputField.classList.add('inputError');
            inputField.tmpValue = inputField.value;
            inputField.value = 'Только числа.';
            result = true;
        }
    }

    function checkFieldsLikeName(fieldId) {
        var regExp = new RegExp('^[a-zа-яё]*$', 'i');
        var inputField = document.getElementById(fieldId);
        if (inputField.value != null && !regExp.test(inputField.value)) {
            inputField.classList.add('inputError');
            inputField.tmpValue = inputField.value;
            inputField.value = 'Только буквы.';
            result = true;
        }
    }
}

function InfoPopup() {
    var mainDiv = createMainDiv();
    addCloseButton();
    addTitle();
    addErrors();
    addInfo();

    function createMainDiv() {
        var mainDiv = document.createElement('div');
        mainDiv.id = 'info';
        mainDiv.classList.add('popup');
        mainDiv.style.display = 'block';
        document.body.appendChild(mainDiv);
        return mainDiv;
    }

    function addCloseButton() {
        var buttonClose = document.createElement('a');
        buttonClose.href = '#';
        buttonClose.onclick = closeInfo;
        mainDiv.appendChild(buttonClose);
    }

    function addTitle() {
        var title = document.createElement('h6');
        title.appendChild(document.createTextNode('Сообщения: '));
        mainDiv.appendChild(title);
    }

    function closeInfo() {
        document.body.removeChild(document.getElementById('info'));
    }

    function addErrors() {
        var infoDiv = document.createElement("div");
        mainDiv.appendChild(infoDiv);
        var errors = document.getElementById("errors");
        for(var i = 0; i < errors.elements.length; i++){
            var label = document.createElement("label");
            label.innerHTML = errors.elements[i].value + "\n";
            label.classList.add("errorLabel");
            infoDiv.appendChild(label);
        }
    }

    function addInfo() {
        var infoDiv = document.createElement("div");
        mainDiv.appendChild(infoDiv);
        var errors = document.getElementById("infoMessages");
        for(var i = 0; i < errors.elements.length; i++){
            var label = document.createElement("label");
            label.innerHTML = errors.elements[i].value + "\n";
            label.classList.add("messageLabel");
            infoDiv.appendChild(label);
        }
    }

}

function getDivFormGroup() {
    var divFormGroup = document.createElement('div');
    divFormGroup.classList.add('form-group');
    return divFormGroup;
}

function clearInputPassword(password) {
    if (password.classList.contains('inputError')) {
        password.classList.remove('inputError');
        password.type = "password";
        password.value = '';
    }
}

function addEvent(htmlElement, eventName, eventFunction) {
    if (htmlElement.addEventListener) {
        htmlElement.addEventListener(eventName, eventFunction, false);
    } else if (htmlElement.attachEvent) {
        htmlElement.attachEvent("on" + eventName, eventFunction);
    } else {
        htmlElement["on" + eventName] = eventFunction;
    }
}

function incorrectDate(inputDate) {
    if (inputDate.value.length == ""){
        return false;
    }
    var regExp = /^[0-9]{4}\W[0-9]{2}\W[0-9]{2}$/;
    var regExpInverse = /^[0-9]{2}\W[0-9]{2}\W[0-9]{4}$/;
    var year;
    var month;
    var day;
    if (regExp.test(inputDate.value)){
        year = inputDate.value.substr(0, 4);
        month = inputDate.value.substr(5,2);
        day = inputDate.value.substr(8, 2);
    } else if (regExpInverse.test(inputDate.value)){
        year = inputDate.value.substr(6, 4);
        month = inputDate.value.substr(3,2);
        day = inputDate.value.substr(0, 2);
    } else {
        inputDate.classList.add('inputError');
        inputDate.tmpValue = inputDate.value;
        inputDate.value = 'Пример: 25.02.1991';
        return true;
    }
    var currentDate = new Date();
    if (year < 1900 || year > currentDate.getFullYear()){
        inputDate.classList.add('inputError');
        inputDate.tmpValue = inputDate.value;
        inputDate.value = 'Неверный год. 1900-' + currentDate.getFullYear();
        return true;
    }
    if (month < 1 || month > 12){
        inputDate.classList.add('inputError');
        inputDate.tmpValue = inputDate.value;
        inputDate.value = 'Неверный месяц. 01-12';
        return true;
    }
    if ( day < 1 || day > 31){
        inputDate.classList.add('inputError');
        inputDate.tmpValue = inputDate.value;
        inputDate.value = 'Неверный день. 01-31';
        return true;
    }
    if (year == currentDate.getFullYear()
        && (month > currentDate.getMonth() + 1 || (month == currentDate.getMonth() + 1 && day > currentDate.getDate()))){
        inputDate.classList.add('inputError');
        inputDate.tmpValue = inputDate.value;
        inputDate.value = 'Будущее время';
        return true;
    }
    inputDate.value = day + "." + month + "." + year;
    return false;
}

function getFormWithCommand(commandValue) {
	var form = document.createElement("form");
	form.method = "POST";
	form.action = "/Front";
	form.enctype = "multipart/form-data";
	addParameterToForm(form, "command", commandValue);
	addExecuteCommandIfException(form);
	return form;
}

function addExecuteCommandIfException(form) {
	var command = document.getElementById("executeCommandIfException");
	if (command !== undefined) {
		addParameterToForm(form, "executeCommandIfException", command.value);
	}
}

function addParameterToForm(form, name, value) {
	var parameter = document.createElement("input");
	parameter.type = 'hidden';
	parameter.name = name;
	parameter.value = value;
	form.appendChild(parameter);
}

function addAllParametersFromForm(formIn, idElement) {
	var elementFrom = document.getElementById(idElement);
	for (var i = 0; i < elementFrom.elements.length; i++) {
		addParameterToForm(formIn, elementFrom.elements[i].name,
				elementFrom.elements[i].value);
	}
}

function submit(form) {
	document.body.appendChild(form);
	form.submit();
}

function allocate(event) {
	with (event.target || event.srcElement) {
        var row = parentNode;
        while (!(row instanceof HTMLTableRowElement)){
            row = row.parentNode;
            if (row instanceof HTMLTableElement){
                return;
            }
        }
		var rowIndex = row.rowIndex;
		if (parentNode.id == -1){
			return;
		}
		var table = row.parentNode.parentNode;
		if (row != table.rows[table.lastSelected]) {
			row.style.backgroundColor = '#ffcc00';
			if (table.lastSelected > 0) {
				table.rows[table.lastSelected].style.backgroundColor = "";
			}
		}
		table.lastSelected = rowIndex;
	}
}

function clearInputElement(element) {
    if (element.classList.contains('inputError')) {
        element.classList.remove('inputError');
        if (element.tmpValue != undefined){
            element.value = element.tmpValue;
        } else {
            element.value = '';
        }
    }
}

function isNotEmail(idField) {
    var regExp = /^[a-z\d]+[a-z\d\u002E\u005F]*[a-z\d]+@([a-z]){2,10}\u002E[a-z]{2,4}$/i;
    var email = document.getElementById(idField);
    if (email.value != "") {
        if (regExp.test(email.value)) {
            return false;
        }
    }
    email.classList.add('inputError');
    email.tmpValue = email.value;
    email.value = 'Пример: email@mail.ru';
    return true;
}

function haveMessages() {
    var errors = document.getElementById("errors");
    if(errors.elements.length > 0){
        return true;
    }
    var infoMessages = document.getElementById("infoMessages");
    if(infoMessages.elements.length > 0){
        return true;
    }
    return false;
}