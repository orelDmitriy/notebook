<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/globalFunction.js"></script>
<link href="${pageContext.request.contextPath}/css/global.css"
	rel="stylesheet" type="text/css">

</head>
<body>
<form id="errors">
    <c:forEach var="error" items="${errors}">
        <input type="hidden" value="${error}">
    </c:forEach>
</form>
<form id="infoMessages">
    <c:forEach var="messag" items="${infoMessages}">
        <input type="hidden" value="${messag}">
    </c:forEach>
</form>
</body>
</html>