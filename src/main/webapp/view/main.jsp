<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="dependencies.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/main.js"></script>
    <link href="${pageContext.request.contextPath}/css/main.css"
          rel="stylesheet" type="text/css">
    <title>Main</title>
</head>
<body>
<div class="container">
    <c:if test="${searchParameters.clear}">
        <div class="row filter">
            <div class="">
                <c:if test="${searchParameters.name != null}">
                    <a href="#" class="disabled">Имя: ${searchParameters.name}</a>
                </c:if>
                <c:if test="${searchParameters.surname != null}">
                    <a href="#" class="disabled">Фамилия: ${searchParameters.surname}</a>
                </c:if>
                <c:if test="${searchParameters.patronymic != null}">
                    <a href="#" class="disabled">Отчество: ${searchParameters.patronymic}</a>
                </c:if>
                <c:if test="${searchParameters.minAge != null}">
                    <a href="#" class="disabled">Возраст от: ${searchParameters.minAge}</a>
                </c:if>
                <c:if test="${searchParameters.maxAge != null}">
                    <a href="#" class="disabled">Возраст до: ${searchParameters.maxAge}</a>
                </c:if>
                <c:if test="${searchParameters.sex != null}">
                    <a href="#" class="disabled">Пол: ${searchParameters.sex}</a>
                </c:if>
                <c:if test="${searchParameters.nationality != null}">
                    <a href="#" class="disabled">Национальность: ${searchParameters.nationality}</a>
                </c:if>
                <c:if test="${searchParameters.maritalStatus != null}">
                    <a href="#" class="disabled">Семейное положение: ${searchParameters.maritalStatus}</a>
                </c:if>
                <c:if test="${searchParameters.country != null}">
                    <a href="#" class="disabled">Страна: ${searchParameters.country}</a>
                </c:if>
                <c:if test="${searchParameters.city != null}">
                    <a href="#" class="disabled">Город: ${searchParameters.city}</a>
                </c:if>
                <c:if test="${searchParameters.street != null}">
                    <a href="#" class="disabled">Улица: ${searchParameters.street}</a>
                </c:if>
                <c:if test="${searchParameters.home != 0}">
                    <a href="#" class="disabled">Дом: ${searchParameters.home}</a>
                </c:if>
                <c:if test="${searchParameters.room != 0}">
                    <a href="#" class="disabled">Квартирра: ${searchParameters.room}</a>
                </c:if>
                <c:if test="${searchParameters.postcode != null}">
                    <a href="#" class="disabled">Почтовый индекс: ${searchParameters.postcode}</a>
                </c:if>
            </div>
            <button onclick="clearFilter();" class="btn btn-success">Очистить</button>
        </div>
    </c:if>
    <div class="row">
        <div class="btn-group btn-group to-left">
            <button onclick="newContact();" class="btn btn-success">Создать</button>
            <button onclick="editContact();" class="btn btn-primary">Редактировать</button>
            <button onclick="deleteContact();" class="btn btn-danger">Удалить</button>
        </div>
        <div class="btn-group btn-group to-right">
            <button onclick="searchContacts();" class="btn btn-info">Поиск</button>
            <button onclick="forwardEmail();" class="btn btn-info">Отправить
                E-mail
            </button>
            <button onclick="logout();" class="btn  btn-warning">Выйти</button>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped smallFont" onclick="allocate(event)" id="tableContacts">
            <tr id="-1">
                <th></th>
                <th>Фамилия Имя Отчество</th>
                <th class="date">День рождения</th>
                <th>Адрес</th>
                <th>Компания</th>
            </tr>
            <c:forEach var="personDto" items="${contactsDto.persons}">
                <tr id="${ personDto.person.idContact}">
                    <td class="check"><input type="checkbox"></td>
                    <td class="fullName">
                        <div><a href="#" onclick="editContact(${personDto.person.idContact})">${ personDto.person}</a></div>
                    </td>
                    <td class="date">${ personDto.person.birthday}</td>
                    <td class="address">
                        <div>${ personDto.address}</div>
                    </td>
                    <td class="company">
                        <div>${ personDto.person.placeOfWork}</div>
                    </td>
                </tr>
            </c:forEach>
            <c:forEach var="i" begin="${contactsDto.size}"
                       end="${navigation.limitForPage - 1}">
                <tr id="-1">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </c:forEach>
        </table>

        <nav>
            <ul class="pager">
                <li class="previous">
                    <a href="#" onclick="gotoPage(1)" <c:if test="${navigation.numberPage == 1}">class="disabled"</c:if> >1</a>
                    <a href="#" onclick="gotoPage(${navigation.numberPage - 1})" <c:if test="${navigation.numberPage == 1}">class="disabled"</c:if>>Предыдущая</a>
                </li>
                <li>
                    <a href="#-1" class="disabled">Всего: ${navigation.amountContactsForUser}
                    </a>
                    <a href="#-1" class="disabled">Текущая: ${navigation.numberPage}
                    </a>
                    <a href="#"
                       onclick="swapLimit(${navigation.limitForPage % 20 + 10});">По ${navigation.limitForPage % 20 + 10}
                    </a>
                </li>
                <li class="next">
                    <a href="#" onclick="gotoPage(${navigation.lastPage});" <c:if test="${navigation.numberPage == navigation.lastPage}">class="disabled"</c:if>>${navigation.lastPage}</a>
                    <a href="#" onclick="gotoPage(${navigation.numberPage + 1});" <c:if test="${navigation.numberPage == navigation.lastPage}">class="disabled"</c:if>>Следующая</a>
                </li>

            </ul>
        </nav>
        <input id="executeCommandIfException" type="hidden" value="ToIndex">
    </div>
</div>
</body>
</html>