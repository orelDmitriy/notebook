<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="dependencies.jsp" %>
<!DOCTYPE html>
<html lang="ru">
<head>

    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/search.js"></script>
    <link href="${pageContext.request.contextPath}/css/search.css"
          rel="stylesheet" type="text/css">

    <title>Поиск</title>
</head>
<body>
<div class="container">
    <div class="main row">
        <div class="leftForm">
            <form id="leftForm">
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" name="name" placeholder="Имя" id="name"
                           maxlength="20" onfocus="clearInputElement(this)">
                </div>
                <div class="form-group">
                    <label for="surname">Фамилия</label>
                    <input type="text" name="surname" placeholder="Фамилия"
                           id="surname" maxlength="20" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="patronymic">Отчество</label>
                    <input type="text" name="patronymic" placeholder="Отчество"
                           id="patronymic" maxlength="20" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="minAge">Возраст от</label>
                    <input type="date" name="minAge" id="minAge" pattern="dd.mm.yyyy"
                           onfocus="clearInputElement(this)" onchange="setMinAge();">
                </div>
                <div class="form-group">
                    <label for="maxAge">Возраст до</label>
                    <input type="date" name="maxAge" id="maxAge" pattern="dd.mm.yyyy"
                           onfocus="clearInputElement(this)" onchange="setMaxAge();">
                </div>
                <div class="form-group">
                    <select name="sex" id="sex" title="Пол">
                        <option>Пол</option>
                        <option>мужской</option>
                        <option <c:if test="${contact.female}">selected='selected'</c:if>>женский</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nationality">Национальность</label>
                    <input type="text" name="nationality" placeholder="Национальность"
                           id="nationality" maxlength="20" onfocus="clearInputElement(this)">
                </div>
            </form>
        </div>
        <div class="rightForm">
            <form id="rightForm">
                <div class="form-group">
                    <select id="maritalStatus" name="maritalStatus" title="Семейное положение">
                        <option>Семейное положение</option>
                        <option>холост</option>
                        <option
                                <c:if test="${contact.married}">selected="selected"</c:if>>женат
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="country" placeholder="Страна"
                           id="country" maxlength="20" onfocus="clearInputElement(this)">
                    <label for="country">Страна</label>
                </div>

                <div class="form-group">
                    <input type="text" name="city" placeholder="Город" id="city"
                           maxlength="20" onfocus="clearInputElement(this)">
                    <label for="city">Город</label>
                </div>

                <div class="form-group">
                    <input type="text" name="street" placeholder="Улица" id="street"
                           maxlength="20" onfocus="clearInputElement(this)">
                    <label for="street">Улица</label>
                </div>

                <div class="form-group">
                    <input type="text" name="home" placeholder="Дом" id="home"
                           maxlength="5" onfocus="clearInputElement(this)">
                    <label for="home">Дом</label>
                </div>

                <div class="form-group">
                    <input type="text" name="room" placeholder="Квартира" id="room"
                           maxlength="5" onfocus="clearInputElement(this)">
                    <label for="room">Квартира</label>
                </div>

                <div class="form-group">
                    <input type="text" name="postcode" placeholder="Почтовый индекс"
                           id="postcode" maxlength="8" onfocus="clearInputElement(this)">
                    <label for="postcode">Почтовый индекс</label>
                </div>
            </form>

        </div>
        <div class="footer row">
            <div class="btn-group full">
                <button onclick="search();" class="btn btn-success">Найти</button>
                <button onclick="cancel();" class="btn btn-warning">Назад</button>
            </div>
        </div>

        <input id="executeCommandIfException" type="hidden" value="ToSearchContacts">
    </div>
</div>
</body>
</html>