<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="dependencies.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/email.js"></script>
    <link href="${pageContext.request.contextPath}/css/email.css"
          rel="stylesheet" type="text/css">
    <title>Send E-mail</title>
</head>
<body>
<div id="container">
    <div class="main row">
        <form id="email">
            <input type="hidden" value="${idRecipients}" id="idRecipients">
            <div class="form-group">
                <label class="control-label">Получатели</label>
                <input type="text" class="form-control" id="emails" value="${emails}" data-tooltip="${emails}" readonly
                       title="${emails}">
            </div>
            <div class="form-group">
                <select id="template" name="template" title="Шаблоны" class="form-control" onchange="setTemplate(this)">
                    <option>Шаблоны</option>
                    <c:forEach var="template" items="${templates}">
                        <option>${template.key}</option>
                    </c:forEach>
                </select>
                <c:forEach var="template" items="${templates}">
                    <input type="hidden" id="message${template.key}" value="${template.value}">
                </c:forEach>
            </div>
            <div class="form-group">
                <input type="text" name="subject" placeholder="Без темы" id="subject" autocomplete="off"
                       maxlength="20" onfocus="clearInputEmailElement(this)" class="form-control">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="message" id="message" maxlength="2000" rows="10" onfocus="clearInputEmailElement(this)"></textarea>
            </div>
        </form>
        <div class="row bottom">
            <div class="btn-group left-button">
                <button onclick="send();" class="btn btn-success">Отправить</button>
            </div>
            <div class="btn-group right-button">
                <button onclick="saveTemplate();" class="btn btn-success">Сохранить шаблон</button>
                <button onclick="deleteTemplate();" class="btn  btn-danger">Удалить</button>
                <button onclick="cancel();" class="btn  btn-warning">Назад</button>
            </div>
        </div>
        <div class="row bottom template-list">
            <div>
                <dl class="dl-horizontal">
                    <dt>&#60;name&#62;</dt>
                    <dd>имя</dd>
                    <dt>&#60;surname&#62;</dt>
                    <dd>фамилия</dd>
                    <dt>&#60;patronymic&#62;</dt>
                    <dd>отчество</dd>
                    <dt>&#60;birthday&#62;</dt>
                    <dd>дата рождения</dd>
                    <dt>&#60;sex&#62;</dt>
                    <dd>пол</dd>
                </dl>
            </div>
            <div>
                <dl class="dl-horizontal ">
                    <dt>&#60;nationality&#62;</dt>
                    <dd>национальность</dd>
                    <dt>&#60;webSite&#62;</dt>
                    <dd>веб сайт</dd>
                    <dt>&#60;placeOfWork&#62;</dt>
                    <dd>место работы</dd>
                    <dt>&#60;familyStatus&#62;</dt>
                    <dd>семейное положение</dd>
                </dl>
            </div>
        </div>
        <input id="executeCommandIfException" type="hidden" value="ToEmail">
    </div>
</div>
</body>
</html>