<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="dependencies.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/registration.js"></script>
<link href="${pageContext.request.contextPath}/css/login.css"
	rel="stylesheet" type="text/css">
<title>Registration</title>
</head>
<body>
	<div class="container-fluid">
		<div class="login">
			<form class="form-horizontal" id="registration">
				<div class="form-group">
					<input type="email" class="form-control" name="email" id="email" onfocus="clearInputElement(this)"
						placeholder="Email" maxlength=45 size=45 value="${email}">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password" id="password" autocomplete="off" onfocus="clearInputPassword(this)"
						placeholder="Пароль" maxlength=45 size=45 value="${password}">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="repeat" id="repeat" autocomplete="off" onfocus="clearInputPassword(this)"
						placeholder="Повторите пароль" maxlength=45 size=45 value="${repeat}">
				</div>
			</form>
			<div class="btn-group outside-form">
				<button onclick="registration();" class="btn btn-success">Зарегистрироваться</button>
				<button onclick="cancel();" class="btn  btn-warning">Назад</button>
			</div>
			<input id="executeCommandIfException" type="hidden" value="ToRegistration">
		</div>
	</div>
</body>
</html>