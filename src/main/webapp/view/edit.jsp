<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="dependencies.jsp" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/edit.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/PhoneFunctions.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/js/AttachmentFunctions.js"></script>
    <link href="${pageContext.request.contextPath}/css/edit.css"
          rel="stylesheet" type="text/css">
    <title>Редактировать контакт</title>
</head>
<body>

<div class="container">
    <div class="main row">

        <div class="left-block">
            <img src="${contact.urlPhoto}" id="photo" onclick="choosePhoto()"
                 width="200" height="200">
            <div class="btn-group full">
                <button onclick="saveContact();" class="btn btn-success">Сохранить</button>
                <button onclick="cancel();" class="btn  btn-warning">Назад</button>
            </div>
        </div>

        <div class="right-block">
            <form id="edit">

                <input type="hidden" id="idContact" value="${contact.idContact}"
                       name="idContact">

                <div class="form-group">
                    <label for="surname">Фамилия</label>
                    <input type="text" name="surname" placeholder="Фамилия" autocomplete="off"
                           id="surname" maxlength="20" value="${contact.surname}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" name="name" placeholder="Имя" id="name" autocomplete="off"
                           maxlength="20" value="${contact.name}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="patronymic">Отчество</label>
                    <input type="text" name="patronymic" placeholder="Отчество" autocomplete="off"
                           id="patronymic" maxlength="20" value="${contact.patronymic}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="birthday">Дата рождения</label>
                    <input type="date" name="birthday" id="birthday" maxlength="20"
                           value="${contact.birthday}" pattern="dd.mm.yyyy" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="sex">Пол</label>
                    <select name="sex" id="sex" title="Пол" onchange="chooseLabelForStatus()">
                        <option>мужской</option>
                        <option <c:if test="${contact.female}">selected='selected'</c:if>>женский</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="nationality">Национальность</label>
                    <input type="text" name="nationality" placeholder="Национальность" autocomplete="off"
                           id="nationality" maxlength="20" value="${contact.nationality}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="maritalStatus">Семейное положение</label>
                    <select id="maritalStatus" name="maritalStatus" title="Семейное положение">
                        <option id="singleMan"
                                <c:if test="${!contact.married}">selected="selected"</c:if>>холост</option>
                        <option id="singleWoman" class="hidden">не замужем</option>
                        <option id="marriedMan"
                                <c:if test="${contact.married}">selected="selected"</c:if>>женат
                        </option>
                        <option id="marriedWoman" class="hidden">замужем</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="webSite">Веб сайт</label>
                    <input type="text" name="webSite" placeholder="Веб сайт" autocomplete="off"
                           id="webSite" maxlength="100" value="${contact.webSite}" onfocus="clearInputElement(this)">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" placeholder="Email" id="email" autocomplete="off"
                           maxlength="44" value="${contact.email}" onfocus="clearInputElement(this)">
                </div>
                <div class="form-group">
                    <label for="placeOfWork">Место работы</label>
                    <input type="text" name="placeOfWork" placeholder="Место работы" autocomplete="off"
                           id="placeOfWork" maxlength="100" value="${contact.placeOfWork}" onfocus="clearInputElement(this)">
                </div>
                <div class="form-group">
                    <label for="country">Страна</label>
                    <input type="text" name="country" placeholder="Страна" autocomplete="off"
                           id="country" maxlength="20" value="${address.country}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="city">Город</label>
                    <input type="text" name="city" placeholder="Город" id="city" autocomplete="off"
                           maxlength="20" value="${address.city}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="street">Улица</label>
                    <input type="text" name="street" placeholder="Улица" id="street" autocomplete="off"
                           maxlength="20" value="${address.street}" onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="home">Дом</label>
                    <input type="text" name="home" placeholder="Дом" id="home" autocomplete="off"
                           maxlength="5" <c:if test="${address.home > 0}">value="${address.home}"</c:if> onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="room">Квартира</label>
                    <input type="text" name="room" placeholder="Квартира" id="room" autocomplete="off"
                           maxlength="5" <c:if test="${address.room > 0}">value="${address.room}"</c:if> onfocus="clearInputElement(this)">
                </div>

                <div class="form-group">
                    <label for="postcode">Почтовый индекс</label>
                    <input type="text" name="postcode" placeholder="Почтовый индекс" autocomplete="off"
                           id="postcode" maxlength="8" value="${address.postcode}" onfocus="clearInputElement(this)">
                </div>
            </form>

            <form method="post" enctype="multipart/form-data" id="formFiles">
                <input type="file" id="loadPhoto" accept="image/jpeg,image/png"
                       name="photo">
            </form>
            <input id="executeCommandIfException" type="hidden" value="GetContacts">

        </div>
    </div>

    <div class="bottom row">
        <div class="phones" id="phonesCheck">
            <div class="btn-group btn-group">
                <button onclick="showPopupPhone();" class="btn btn-success">Добавить телефон</button>
                <button onclick="editPhonePopup();" class="btn btn-primary">Редактировать</button>
                <button onclick="deletePhones();" class="btn btn-danger">Удалить</button>
            </div>
            <div class="table">
                <table id="phones" class="table table-striped smallFont"
                       onclick="allocate(event)">
                    <tr id="-1">
                        <th class="check"></th>
                        <th class="phoneNumber">Номер</th>
                        <th class="description">Тип</th>
                        <th class="comment">Комментарий</th>
                    </tr>

                    <c:forEach var="phone" items="${phones}">
                        <tr id="${phone.idPhone}p">
                            <td class='check'><input type="checkbox" title="select"></td>
                            <td class="phoneNumber"><div>${phone.fullNumber}</div></td>
                            <td class="description"><div>${phone.type}</div></td>
                            <td class='comment'><div>${phone.comment}</div></td>
                        </tr>
                    </c:forEach>
                    <c:forEach var="i" begin="${phonesSize + 1}" end="8">
                        <tr id="-1">
                            <td class='check'></td>
                            <td class="phoneNumber"></td>
                            <td class="description"></td>
                            <td class='comment'></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>


        <div class="attachments" id="attachmentsCheck">

            <div class="btn-group btn-group">
                <button onclick="showAttachmentPopup();" class="btn btn-success">Прикрепить файл</button>
                <button onclick="editAttachmentPopup();" class="btn btn-primary">Редактировать</button>
                <button onclick="deleteAttachments();" class="btn btn-danger">Удалить</button>
            </div>
            <div class="table" onclick="allocate(event)">
                <table id="attachments" class="table table-striped smallFont">
                    <tr id="-1">
                        <th class="check"></th>
                        <th class="fileName">Имя файла</th>
                        <th class="date">Дата</th>
                        <th class="comment">Комментарий</th>
                    </tr>

                    <c:forEach var="attachment" items="${attachments}">
                        <tr id="${attachment.idAttachment}">
                            <td class="check"><input type="checkbox" title="select"></td>
                            <td class="fileName"><div><a href="${attachment.urlAttachment}" download="${attachment.simpleFileName}">${attachment.simpleFileName}</a></div></div></td>
                            <td class="date"><div>${attachment.loadDate}</div></td>
                            <td class="comment"><div>${attachment.comment}</div></td>
                        </tr>
                    </c:forEach>
                    <c:forEach var="i" begin="${attachmentsSize + 1}" end="8">
                        <tr id="-1">
                            <td class="check"></td>
                            <td class="fileName"></td>
                            <td class="date"></td>
                            <td class="comment"></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>

        </div>
    </div>
</div>
</body>
</html>