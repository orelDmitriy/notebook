<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="dependencies.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/login.js"></script>
<link href="${pageContext.request.contextPath}/css/login.css"
	rel="stylesheet" type="text/css">
<title>Login</title>
</head>
<body>
	<div class="container-fluid">
		<div class="login">
			<form class="form-horizontal" id="login">
                <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" onfocus="clearInputElement(this)"
                           placeholder="Email" maxlength=45 size=45>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" id="password" autocomplete="off" onfocus="clearInputPassword(this)"
                           placeholder="Пароль" maxlength=45 size=45>
                </div>
			</form>
			<div class="btn-group outside-form">
				<button onclick="login();" class="btn btn-success">Войти</button>
				<button onclick="toRegistration();" class="btn btn-primary">Регистрация</button>
			</div>
			<input id="executeCommandIfException" type="hidden" value="ToIndex">
		</div>
	</div>
</body>
</html>