package ui.controllers;

import domain.commands.Command;
import exceptions.AccessException;
import exceptions.DaoException;
import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.log4j.Logger;
import utils.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet({"/Front"})
public class Front extends HttpServlet {
    private static final long serialVersionUID = 2538309742555671856L;
    private static final boolean accessMode = true;

    private Logger logger = Logger.getLogger(Front.class.getName());

    @Override
    protected void service(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        String nextPage;
        try {
            formatRequest(request);
            nextPage = executeCommand(request, response);
        } catch (FileUploadException e) {
            addErrorMessage(request, e, "Размер файла превышает 1мб.");
            nextPage = chooseNextPage(request, response);
        } catch (Exception e) {
            addErrorMessage(request, e, "Непредвиденное поведение.");
            nextPage = "login";
        }
        forward(request, response, nextPage);
    }

    private void formatRequest(HttpServletRequest request) throws FileUploadException {
        FormatterRequest formatterRequest = new FormatterRequest(request);
        formatterRequest.format();
        formatterRequest.writeToLogAndConsoleAllAttribute();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            AutoSenderEmail autoSenderEmail = new AutoSenderEmail();
            autoSenderEmail.start();
        } catch (Exception e) {
            logger.error("Ошибка инициализации автоотправки email.", e);
        }
        FileUtil.init(getServletContext().getRealPath(""));
    }

    private String executeCommand(HttpServletRequest request,
                                  HttpServletResponse response) {
        String nextPage;
        try {
            Command command = createCommand((String) request.getAttribute("command"));
            checkSessionStateAndAccess(request, command);
            nextPage = command.execute(request, response);
        } catch (IncorrectDataException e) {
            e.printStackTrace();
            addErrorMessage(request, e, "Некоректные данные.");
            nextPage = chooseNextPage(request, response);
        } catch (AccessException e) {
            e.printStackTrace();
            addErrorMessage(request, e, "Сессия закрыта");
            nextPage = "login";
        } catch (DaoException e) {
            e.printStackTrace();
            addErrorMessage(request, e, "Ошибка обращения к базе данных.");
            nextPage = chooseNextPage(request, response);
        } catch (ExecutingCommandsException e) {
            e.printStackTrace();
            addErrorMessage(request, e, "Ошибка выполнения команды.");
            nextPage = chooseNextPage(request, response);
        }
        return nextPage;
    }

    private Command createCommand(String command) {
        try {
            return (Command) Class.forName("domain.commands." + command, true,
                    this.getClass().getClassLoader()).newInstance();
        } catch (ReflectiveOperationException e) {
            String message = "Команда не найдена " + command;
            logger.error(message, e);
            throw new ExecutingCommandsException(message);
        }
    }

    private void checkSessionStateAndAccess(HttpServletRequest request,
                                            Command command) {
        if (accessMode) {
            if ((new SessionUtil()).isClosed(request) && !command.isFreeAccess()) {
                throw new AccessException("Доступ закрыт.");
            }
        }
    }

    private void addErrorMessage(HttpServletRequest request, Throwable e, String message) {
        logger.error(message, e);
        RequestUtils.addErrorMessage(request, message);
        RequestUtils.addErrorMessage(request, e.getMessage());
    }

    private String chooseNextPage(HttpServletRequest request, HttpServletResponse response) {
        String command = (String) request.getAttribute("executeCommandIfException");
        if (command == null) {
            command = "ToIndex";
        }
        request.setAttribute("command", command);
        return executeCommand(request, response);
    }

    private void forward(HttpServletRequest request,
                         HttpServletResponse response, String nextPage) {
        try {
            this.getServletContext()
                    .getRequestDispatcher("/view/" + nextPage + ".jsp")
                    .forward(request, response);
        } catch (ServletException | IOException e) {
            String message = "Ошибка перехода на страницу " + nextPage + ".jsp";
            logger.error(message, e);
            throw new ExecutingCommandsException(message);
        }
    }

}
