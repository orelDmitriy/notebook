package domain.models;

import org.apache.commons.lang3.StringUtils;
import utils.ContactFilesService;
import utils.FileUtil;

import java.io.Serializable;

public class Contact extends Person implements Serializable {
    private static final long serialVersionUID = -2279093033654261942L;

    private int idUser;
    private String nationality;
    private Sex sex;
    private MaritalStatus maritalStatus;
    private String webSite;
    private String email;
    private String namePhoto;

    public Contact() {
        super();
        idUser = -1;
        nationality = "";
        webSite = "";
        email = "";
        namePhoto = "";
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getNationality() {
        return StringUtils.defaultString(nationality);
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getWebSite() {
        return StringUtils.defaultString(webSite);
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getEmail() {
        return StringUtils.defaultString(email);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNamePhoto() {
        return namePhoto;
    }

    public void setNamePhoto(String namePhoto) {
        this.namePhoto = namePhoto;
    }

    public String getUrlPhoto() {
        String localPath = ContactFilesService.getLocalPhotoPath(idContact, namePhoto);
        if (FileUtil.exists(localPath)) {
            return FileUtil.getPathInServer(localPath);
        }
        return "images/noAvatar.jpg";
    }

    public boolean isFemale() {
        if (sex == Sex.женский) {
            return true;
        }
        return false;
    }

    public boolean isMarried() {
        if (maritalStatus == MaritalStatus.женат) {
            return true;
        }
        return false;
    }

    public boolean isExist() {
        return idContact > 0;
    }

    public boolean hasUpdatePhoto() {
        return StringUtils.isNotEmpty(namePhoto);
    }

    public enum Sex {
        мужской, женский;

        @Override
        public String toString() {
            return this.name();
        }
    }

    public enum MaritalStatus {
        женат, холост;

        @Override
        public String toString() {
            return this.name();
        }
    }
}
