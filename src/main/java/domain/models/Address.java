package domain.models;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

import static org.apache.commons.lang3.StringUtils.defaultString;

public class Address implements Serializable {
    private static final long serialVersionUID = 3544374670746617562L;
    private int idAddress;
    private int idContact;
    private String country;
    private String city;
    private String street;
    private int home;
    private int room;
    private String postcode;

    public Address() {
        idAddress = -1;
        idContact = -1;
        country = "";
        city = "";
        street = "";
        home = 0;
        room = 0;
        postcode = "";
    }

    public int getIdContact() {
        return idContact;
    }

    public void setIdContact(int idContact) {
        this.idContact = idContact;
    }

    public int getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(int idAddress) {
        this.idAddress = idAddress;
    }

    public String getCountry() {
        return defaultString(country);
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return defaultString(city);
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return defaultString(street);
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHome() {
        return home;
    }

    public void setHome(String home) {
        if (StringUtils.isNotEmpty(home)) {
            setHome(Integer.parseInt(home));
        } else {
            setHome(0);
        }
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(String room) {
        if (StringUtils.isNotEmpty(room)) {
            setRoom(Integer.parseInt(room));
        } else {
            setRoom(0);
        }
    }

    public String getPostcode() {
        return defaultString(postcode);
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setHome(int home) {
        this.home = home;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    @Override
    public String toString() {
        String result = "";
        if (getCountry().length() > 0) {
            result += country;
        }
        if (getCity().length() > 0) {
            result += " " + city;
        }
        if (getStreet().length() > 0) {
            result += " ул. " + street;
        }
        if (home > 0) {
            result += " д. " + home;
        }
        if (room > 0) {
            result += " к. " + room;
        }
        if (getPostcode().length() > 0) {
            result += " п/и " + postcode;
        }
        return result;
    }
}
