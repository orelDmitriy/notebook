package domain.models;

import java.io.Serializable;

public class Phone implements Serializable {
    private static final long serialVersionUID = -5825798113412172164L;
    private int idPhone;
    private int idContact;
    private int countryCode;
    private int operatorCode;
    private int number;
    private TypePhone type;
    private String comment;

    public Phone() {
        type = TypePhone.мобильный;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public int getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(int operatorCode) {
        this.operatorCode = operatorCode;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getIdPhone() {
        return idPhone;
    }

    public void setIdPhone(int idPhone) {
        this.idPhone = idPhone;
    }

    public int getIdContact() {
        return idContact;
    }

    public void setIdContact(int idContact) {
        this.idContact = idContact;
    }

    public TypePhone getType() {
        return type;
    }

    public void setType(TypePhone type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFullNumber() {
        return "+" + countryCode + "(" + operatorCode + ")" + number;
    }

    public enum TypePhone {
        домашний, мобильный;

        @Override
        public String toString() {
            return this.name();
        }
    }

}
