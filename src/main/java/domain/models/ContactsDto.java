package domain.models;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ContactsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<PersonDto> persons;
	private int size;

	public ContactsDto() {
		persons = new LinkedList<>();
		size = 0;
	}

	public int getSize() {
		return size;
	}

	public List<PersonDto> getPersons() {
		return persons;
	}

	public void setPersons(List<PersonDto> persons) {
		this.persons = persons;
		size = persons.size();
	}

}
