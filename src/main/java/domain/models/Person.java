package domain.models;

import java.io.Serializable;
import java.time.LocalDate;

public class Person implements Serializable {

    private static final long serialVersionUID = -4172792634896901991L;

    protected int idContact;
    protected String name;
    protected String surname;
    protected String patronymic;
    protected LocalDate birthday;
    protected String placeOfWork;

    public Person() {
        idContact = -1;
        name = "";
        surname = "";
        patronymic = "";
        birthday = null;
        placeOfWork = "";
    }

    public int getIdContact() {
        return idContact;
    }

    public void setIdContact(String idContact) {
        try {
            this.idContact = Integer.parseInt(idContact);
        } catch (Exception e) {
            this.idContact = -1;
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public String getPlaceOfWork() {
        return placeOfWork;
    }

    public void setPlaceOfWork(String placeOfWork) {
        this.placeOfWork = placeOfWork;
    }

    public void setIdContact(int idContact) {
        this.idContact = idContact;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        String result = "";
        if (surname != null) {
            result += surname;
        }
        if (name != null) {
            result += " " + name;
        } else {
            return null;
        }
        if (patronymic != null) {
            result += " " + patronymic;
        }
        return result;
    }

}
