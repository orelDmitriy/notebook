package domain.models;

import java.io.Serializable;

import domain.models.Address;
import domain.models.Person;

public class PersonDto implements Serializable {

	private static final long serialVersionUID = -2500094977477221158L;
	private Address address;
    private Person person;

    public Person getPerson() {return  person;}
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

    public void setPerson(Person person) {
        this.person = person;
    }

}
