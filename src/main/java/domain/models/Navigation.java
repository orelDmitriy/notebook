package domain.models;

import java.io.Serializable;

public class Navigation implements Serializable {
    private static final long serialVersionUID = -5825798113417672164L;
    private int limitForPage;
    private int numberPage;
    private int amountContactsForUser;
    private int lastPage;

    public Navigation() {
        limitForPage = 10;
        amountContactsForUser = 0;
        numberPage = 1;
        lastPage = 1;
    }

    public int getLastPage() {
        return lastPage;
    }

    private void setLastPage(int lastPage) {
        if (lastPage > 0) {
            this.lastPage = lastPage;
            if (numberPage > lastPage){
                numberPage = lastPage;
            }
        }
    }

    public int getNumberPage() {
        return numberPage;
    }

    public void setNumberPage(int numberPage) {
        if (numberPage > 0 && numberPage <= lastPage) {
            this.numberPage = numberPage;
        }
    }

    public int getLimitForPage() {
        return limitForPage;
    }

    public void setLimitForPage(int limitForPage) {
        if (limitForPage == 10 || limitForPage == 20) {
            if (this.limitForPage != limitForPage) {
                float tmpNumber = numberPage;
                float tmpLastPage = lastPage;
                if (limitForPage == 20) {
                    setLastPage((int) Math.ceil(tmpLastPage / 2));
                    setNumberPage((int) Math.ceil(tmpNumber / 2));
                } else {
                    setLastPage((int) (tmpLastPage * 2 - 1));
                    setNumberPage((int) (tmpNumber * 2 - 1));
                }
                this.limitForPage = limitForPage;
            }
        }
    }

    public int getAmountContactsForUser() {
        return amountContactsForUser;
    }

    public void setAmountContactsForUser(int amountContactsForUser) {
        if (amountContactsForUser >= 0) {
            this.amountContactsForUser = amountContactsForUser;
            setLastPage((int) Math.ceil((double) amountContactsForUser / (double) limitForPage));
            if (numberPage > lastPage){
                numberPage = lastPage;
            }
        }
    }

    public void clear() {
        numberPage = 1;
        lastPage = 1;
    }
}
