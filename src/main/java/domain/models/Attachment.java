package domain.models;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;
import utils.ContactFilesService;
import utils.FileUtil;
import utils.InputInspector;
import exceptions.IncorrectDataException;

public class Attachment implements Serializable {
	private static final long serialVersionUID = 4365468917180065652L;
	private int idAttachment;
	private int idContact;
	private String fileName;
	private LocalDate loadDate;
	private String comment;
	
	public Attachment() {
        idAttachment = -1;
        idContact = -1;
        fileName = "";
        comment = "";
    }
	
	public LocalDate getLoadDate() {
		return loadDate;
	}

	public void setLoadDate(LocalDate loadDate) {
		this.loadDate = loadDate;
	}

	public int getIdAttachment() {
		return idAttachment;
	}

	public int getIdContact() {
		return idContact;
	}

	public String getFileName() {
		return fileName;
	}
	
	public String getSimpleFileName(){
		return ContactFilesService.getSimpleName(fileName);
	}
	
	public String getUrlAttachment() {
		return FileUtil.getPathInServer(ContactFilesService.getLocalAttachmentPath(idContact, fileName));
	}

	public String getComment() {
		return comment;
	}

    public void setIdAttachment(int idAttachment) {
        this.idAttachment = idAttachment;
    }

    public void setIdContact(int idContact) {
        this.idContact = idContact;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
	public String toString(){
		String result = idAttachment + " " + idContact + " " + fileName + " " + loadDate + " " + comment;
		return result;
	}

}
