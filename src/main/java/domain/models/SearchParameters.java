package domain.models;

import org.apache.commons.lang3.StringUtils;
import utils.InputInspector;

import java.io.Serializable;
import java.time.LocalDate;

public class SearchParameters implements Serializable {
    private static final long serialVersionUID = -5825798143412172164L;
    private String name;
    private String surname;
    private String patronymic;
    private LocalDate minAge;
    private LocalDate maxAge;
    private String sex;
    private String nationality;
    private String maritalStatus;
    private String country;
    private String city;
    private String street;
    private int home;
    private int room;
    private String postcode;

    private InputInspector inputInspector = new InputInspector();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (inputInspector.isSimpleName(name)) {
            this.name = StringUtils.lowerCase(name);
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        if (inputInspector.isSimpleName(surname)) {
            this.surname = StringUtils.lowerCase(surname);
        }
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        if (inputInspector.isSimpleName(patronymic)) {
            this.patronymic = StringUtils.lowerCase(patronymic);
        }
    }

    public LocalDate getMinAge() {
        return minAge;
    }

    public void setMinAge(LocalDate minAge) {
        this.minAge = minAge;
    }

    public LocalDate getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(LocalDate maxAge) {
        this.maxAge = maxAge;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        String lowerCaseSex = StringUtils.lowerCase(sex);
        if ("мужской".equals(lowerCaseSex) || "женский".equals(lowerCaseSex)) {
            this.sex = lowerCaseSex;
        }
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        if (inputInspector.isSimpleName(nationality)) {
            this.nationality = StringUtils.lowerCase(nationality);
        }
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        String lowerCaseMaritalStatus = StringUtils.lowerCase(maritalStatus);
        if ("холост".equals(lowerCaseMaritalStatus) || "женат".equals(lowerCaseMaritalStatus)) {
            this.maritalStatus = lowerCaseMaritalStatus;
        }
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (inputInspector.isSimpleName(country)) {
            this.country = StringUtils.lowerCase(country);
        }
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if (inputInspector.isSimpleName(city)) {
            this.city = StringUtils.lowerCase(city);
        }
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        if (inputInspector.isSimpleName(street)) {
            this.street = StringUtils.lowerCase(street);
        }
    }

    public int getHome() {
        return home;
    }

    public void setHome(String home) {
        try {
            this.home = Integer.parseInt(home);
        } catch (Exception e) {
            this.home = 0;
        }
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(String room) {
        try {
            this.room = Integer.parseInt(room);
        } catch (Exception e) {
            this.room = 0;
        }
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        if (inputInspector.isPostcode(street)) {
            this.postcode = StringUtils.lowerCase(postcode);
        }
    }

    public boolean isClear() {
        return StringUtils.isNotEmpty(name) || StringUtils.isNotEmpty(surname) || StringUtils.isNotEmpty(patronymic)
                || StringUtils.isNotEmpty(sex) || StringUtils.isNotEmpty(nationality) || StringUtils.isNotEmpty(maritalStatus)
                || StringUtils.isNotEmpty(country) || StringUtils.isNotEmpty(city) || StringUtils.isNotEmpty(street)
                || StringUtils.isNotEmpty(postcode) || (home != 0) || (room != 0)
                || minAge != null || maxAge != null;
    }

    @Override
    public String toString(){
        String result = "";
        result+= "name: " + name;
        result+= "surname: " + surname;
        result+= "patronymic: " + patronymic;
        result+= "minAge: " + minAge;
        result+= "maxAge: " + maxAge;
        result+= "sex: " + sex;
        result+= "nationality: " + nationality;
        result+= "maritalStatus: " + maritalStatus;
        result+= "country: " + country;
        result+= "city: " + city;
        result+= "street: " + street;
        result+= "home: " + home;
        result+= "room: " + room;
        result+= "postcode: " + postcode;
        return result;
    }

}
