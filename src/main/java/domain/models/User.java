package domain.models;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = -8371091858506064331L;
    private int idUser;
    private String email;
    private String password;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return email;
    }

}
