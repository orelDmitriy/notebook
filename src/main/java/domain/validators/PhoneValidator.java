package domain.validators;

import domain.models.Phone;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.InputInspector;

import java.io.Serializable;

public class PhoneValidator {
    private Phone phone;
    private InputInspector inputInspector;

    public PhoneValidator() {
        this(new Phone());
    }

    public PhoneValidator(Phone phone) {
        this.phone = phone;
        inputInspector = new InputInspector();
    }

    public void setCountryCode(int countryCode) {
        if (countryCode > 1 && countryCode < 10000) {
            phone.setCountryCode(countryCode);
        } else {
            throw new IncorrectDataException("Неверный код страны: "
                    + countryCode);
        }
    }

    public void setOperatorCode(int operatorCode) {
        if (operatorCode > 1 && operatorCode < 100000) {
            phone.setOperatorCode(operatorCode);
        } else {
            throw new IncorrectDataException("Некоректный код оператора: "
                    + operatorCode);
        }
    }

    public void setNumber(int number) {
        if (number > 0 && number < 10000000) {
            phone.setNumber(number);
        } else {
            throw new IncorrectDataException("Некоректный номер: "
                    + number);
        }
    }

    public void setIdPhone(int idPhone) {
        if (inputInspector.isId(idPhone)) {
            phone.setIdPhone(idPhone);
        } else {
            throw new IncorrectDataException("Некоректный idPhone: "
                    + idPhone);
        }
    }

    public void setIdContact(int idContact) {
        if (inputInspector.isId(idContact)) {
            phone.setIdContact(idContact);
        } else {
            throw new IncorrectDataException("Некоректный id контакта: "
                    + idContact);
        }
    }

    public void setType(String type) {
        if ("домашний".equals(type)) {
            phone.setType(Phone.TypePhone.домашний);
        } else {
            phone.setType(Phone.TypePhone.мобильный);
        }
    }

    public void setComment(String comment) {
        phone.setComment(StringUtils.defaultString(comment));

    }

    public void setFullNumber(String fullNumber) {
        String countryCode = fullNumber.substring(1, fullNumber.indexOf('('));
        String operatorCode = fullNumber.substring(fullNumber.indexOf('(') + 1, fullNumber.indexOf(')'));
        String number = fullNumber.substring(fullNumber.indexOf(')') + 1);
        setCountryCode(Integer.parseInt(countryCode));
        setOperatorCode(Integer.parseInt(operatorCode));
        setNumber(Integer.parseInt(number));
    }

    public Phone getPhone() {
        return phone;
    }

}
