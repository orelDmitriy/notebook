package domain.validators;

import domain.models.Contact;
import domain.models.Person;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.ContactFilesService;
import utils.FileUtil;

import java.io.Serializable;

public class ContactValidator extends PersonValidator {
    private Contact contact;

    public ContactValidator() {
        this(new Contact());
    }

    public ContactValidator(Contact contact) {
        super(contact);
        this.contact = contact;
    }

    public void setIdUser(int idUser) {
        if (inputInspector.isId(idUser)) {
            contact.setIdUser(idUser);
        } else {
            throw new IncorrectDataException("Неверный idUser: "
                    + idUser);
        }
    }

    public void setSex(String sex) {
        if ("мужской".equals(sex)) {
            contact.setSex(Contact.Sex.мужской);
        } else {
            contact.setSex(Contact.Sex.женский);
        }
    }

    public void setNationality(String nationality) {
        if (inputInspector.isSimpleName(nationality)) {
            contact.setNationality(nationality);
        } else {
            contact.setNationality("");
        }
    }

    public void setMaritalStatus(String status) {
        if ("холост".equals(status) || "не замужем".equals(status)) {
            contact.setMaritalStatus(Contact.MaritalStatus.холост);
        } else {
            contact.setMaritalStatus(Contact.MaritalStatus.женат);
        }
    }

    public void setWebSite(String webSite) {
        if (inputInspector.isUrl(webSite) && webSite.length() <= 100) {
            contact.setWebSite(webSite);
        } else {
            contact.setWebSite("");
        }
    }

    public void setEmail(String email) {
        if (inputInspector.isEmail(email)) {
            contact.setEmail(email);
        } else {
            if (StringUtils.isNotEmpty(email)) {
                new IncorrectDataException("Неверный емаил: " + email);
            }
        }
    }

    public void setNamePhoto(String namePhoto) {
        if (StringUtils.isNotEmpty(namePhoto)) {
            contact.setNamePhoto(namePhoto);
        }
    }

    public Contact getContact() {
        return contact;
    }
}
