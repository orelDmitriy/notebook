package domain.validators;

import domain.models.Attachment;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.InputInspector;

import java.time.LocalDate;

public class AttachmentValidator {

    private Attachment attachment;
    private InputInspector inputInspector;

    public AttachmentValidator() {
        this(new Attachment());
    }

    public AttachmentValidator(Attachment attachment) {
        this.attachment = attachment;
        inputInspector = new InputInspector();
    }

    public void setIdAttachment(int idAttachment) {
        if (inputInspector.isId(idAttachment)) {
            attachment.setIdAttachment(idAttachment);
        } else {
            attachment.setIdAttachment(-1);
        }
    }

    public void setIdContact(int idContact) throws IncorrectDataException {
        if (inputInspector.isId(idContact)) {
            attachment.setIdContact(idContact);
        } else {
            throw new IncorrectDataException("Неверный id контакта: "
                    + idContact);
        }
    }

    public void setFileName(String fileName) throws IncorrectDataException {
        if (StringUtils.isNotEmpty(fileName)) {
            attachment.setFileName(fileName);
        } else {
            throw new IncorrectDataException("Некоректное имя файла: " + fileName);
        }
    }

    public void setComment(String comment) {
        attachment.setComment(StringUtils.defaultString(comment));
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setLoadDate(LocalDate loadDate) {
        attachment.setLoadDate(loadDate);
    }
}
