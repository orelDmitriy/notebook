package domain.validators;

import domain.models.User;
import exceptions.IncorrectDataException;
import utils.InputInspector;

import java.io.Serializable;

public class UserValidator {
    private User user;

    private InputInspector inputInspector;

    public UserValidator() {
        this(new User());
    }

    public UserValidator(User user) {
        this.user = user;
        inputInspector = new InputInspector();
    }

    public void setIdUser(int idUser) {
        if (inputInspector.isId(idUser)) {
            user.setIdUser(idUser);
        } else {
            throw new IncorrectDataException("Некоректный idUser: " + idUser);
        }
    }

    public void setEmail(String email) throws IncorrectDataException {
        if (inputInspector.isEmail(email)) {
            user.setEmail(email);
        } else {
            throw new IncorrectDataException("Некоректный email: " + email);
        }
    }

    public void setPassword(String password) {
        if (inputInspector.isPassword(password)) {
            user.setPassword(password);
        } else {
            throw new IncorrectDataException("Некоректный парль: " + password);
        }
    }

    public User getUser() {
        return user;
    }

}
