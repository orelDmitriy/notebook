package domain.validators;

import domain.models.Person;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.InputInspector;

import java.io.Serializable;
import java.time.LocalDate;

public class PersonValidator {

    protected InputInspector inputInspector;
    protected Person person;

    public PersonValidator() {
        this(new Person());
    }

    public PersonValidator(Person person) {
        this.person = person;
        inputInspector = new InputInspector();
    }

    public void setIdContact(String idContact) {
        try {
            setIdContact(Integer.parseInt(idContact));
        } catch (NumberFormatException e) {
            throw new IncorrectDataException("Неверный idContact: "
                    + idContact);
        }
    }

    public void setIdContact(int idContact) {
        if (inputInspector.isId(idContact)) {
            person.setIdContact(idContact);
        } else {
            person.setIdContact(-1);
        }
    }

    public void setName(String name) {
        if (inputInspector.isSimpleName(name) && name.length() > 2) {
            person.setName(name);
        } else {
            throw new IncorrectDataException("Некоректное имя: " + name);
        }
    }

    public void setSurname(String surname) {
        if (inputInspector.isSimpleName(surname)) {
            person.setSurname(surname);
        } else {
            person.setSurname("");
        }
    }

    public void setPatronymic(String patronymic) {
        if (inputInspector.isSimpleName(patronymic)) {
            person.setPatronymic(patronymic);
        } else {
            person.setPatronymic("");
        }
    }

    public void setBirthday(LocalDate birthday) {
        if (birthday == null) {
            return;
        }
        if (birthday.compareTo(LocalDate.now()) <= 0) {
            person.setBirthday(birthday);
        } else {
            throw new IncorrectDataException("День рождения не может быть больше текущей даты.");
        }
    }

    public void setPlaceOfWork(String placeOfWork) {
        if (StringUtils.isNotEmpty(placeOfWork) && placeOfWork.length() <= 100) {
            person.setPlaceOfWork(placeOfWork);
        } else {
            person.setPlaceOfWork("");
        }
    }

    public Person getPerson() {
        return person;
    }
}
