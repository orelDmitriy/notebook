package domain.validators;

import domain.models.Address;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.InputInspector;

import java.io.Serializable;

import static org.apache.commons.lang3.StringUtils.defaultString;

public class AddressValidator {
    private Address address;
    private InputInspector inputInspector;

    public AddressValidator() {
        this(new Address());
    }

    public AddressValidator(Address address) {
        this.address = address;
        inputInspector = new InputInspector();

    }

    public void setIdContact(int idContact) {
        if (inputInspector.isId(idContact)) {
            address.setIdContact(idContact);
        } else {
            throw new IncorrectDataException("Некоректный id контакта.");
        }
    }

    public void setIdAddress(int idAddress) {
        if (inputInspector.isId(idAddress)) {
            address.setIdAddress(idAddress);
        } else {
            address.setIdAddress(-1);
        }
    }

    public void setCountry(String country) {
        if (inputInspector.isSimpleName(country)) {
            address.setCountry(country);
        } else {
            address.setCountry("");
        }
    }

    public void setCity(String city) {
        if (inputInspector.isSimpleName(city)) {
            address.setCity(city);
        } else {
            address.setCity("");
        }
    }

    public void setStreet(String street) {
        if (inputInspector.isSimpleName(street)) {
            address.setStreet(street);
        } else {
            address.setStreet("");
        }
    }

    public void setHome(int home) {
        if (home > 0) {
            address.setHome(home);
        } else {
            address.setHome(0);
        }
    }

    public void setRoom(int room) {
        if (room > 0) {
            address.setRoom(room);
        } else {
            address.setRoom("");
        }
    }

    public void setPostcode(String postcode) {
        if (inputInspector.isPostcode(postcode)) {
            address.setPostcode(postcode);
        } else {
            address.setPostcode("");
        }
    }

    public void setHome(String home) {
        if (StringUtils.isNotEmpty(home)) {
            setHome(Integer.parseInt(home));
        } else {
            setHome(0);
        }
    }

    public void setRoom(String room) {
        if (StringUtils.isNotEmpty(room)) {
            setRoom(Integer.parseInt(room));
        } else {
            setRoom(0);
        }
    }
}
