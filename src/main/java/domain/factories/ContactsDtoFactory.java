package domain.factories;

import dao.AddressDao;
import dao.ContactDao;
import dao.impl.MysqlAddressDao;
import dao.impl.MysqlContactDao;
import domain.models.Address;
import domain.models.Contact;
import domain.models.ContactsDto;
import domain.models.Navigation;
import domain.models.PersonDto;
import domain.validators.PersonValidator;
import exceptions.AccessException;
import exceptions.ExecutingCommandsException;
import utils.SessionUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

public class ContactsDtoFactory {

    private static ContactDao contactDao = new MysqlContactDao();
    private static AddressDao addressDao = new MysqlAddressDao();

    private SessionUtil sessionUtil;
    private Navigation navigation;

    public ContactsDtoFactory() {
        sessionUtil = new SessionUtil();
    }

    public ContactsDto getContactsDto(HttpServletRequest request)
            throws ExecutingCommandsException, AccessException {
        navigation = sessionUtil.getNavigation(request);
        ContactsDto contactsDto = new ContactsDto();
        contactsDto.setPersons(getPersonsForUser(request));
        updateAmountContacts(request);
        return contactsDto;
    }

    private List<PersonDto> getPersonsForUser(HttpServletRequest request) {
        List<PersonDto> persons = null;
        List<Contact> contacts = contactDao.getAllByIdUserWithParameters(
                sessionUtil.getUser(request).getIdUser(),
                navigation.getNumberPage(),
                navigation.getLimitForPage(),
                sessionUtil.getSearchParameters(request)
        );
        persons = getPersonsFromContacts(contacts);
        return persons;
    }

    private List<PersonDto> getPersonsFromContacts(List<Contact> contacts){
        List<PersonDto> persons = new LinkedList<PersonDto>();
        PersonDto person;
        Address address;
        for (Contact contact : contacts) {
            person = new PersonDto();
            PersonValidator personValidator = new PersonValidator();
            personValidator.setIdContact(contact.getIdContact());
            personValidator.setName(contact.getName());
            personValidator.setSurname(contact.getSurname());
            personValidator.setPatronymic(contact.getPatronymic());
            personValidator.setBirthday(contact.getBirthday());
            personValidator.setPlaceOfWork(contact.getPlaceOfWork());
            address = addressDao.getByIdContact(contact.getIdContact());
            person.setAddress(address);
            person.setPerson(personValidator.getPerson());
            persons.add(person);
        }
        return persons;
    }

    private void updateAmountContacts(HttpServletRequest request){
        navigation.setAmountContactsForUser(contactDao.getAmountContactsForUser(sessionUtil.getUser(request).getIdUser(),
                sessionUtil.getSearchParameters(request)));
    }
}
