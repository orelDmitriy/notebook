package domain.factories;

import domain.models.Contact;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.StringRenderer;
import utils.ConverterDate;

import java.util.List;

public class StringTemplateFactory {

    public String fillMessage(Contact contact, String message) {
        ST st = new ST(message);
        st.add("name", contact.getName());
        st.add("surname", contact.getSurname());
        st.add("patronymic", contact.getPatronymic());
        st.add("birthday", ConverterDate.format(contact.getBirthday()));
        st.add("sex", contact.getSex());
        st.add("nationality", contact.getNationality());
        st.add("webSite", contact.getWebSite());
        st.add("placeOfWork", contact.getPlaceOfWork());
        st.add("email", contact.getEmail());
        st.add("familyStatus", contact.getMaritalStatus());
        return st.render();
    }

    public String createMessageWithBirthdays(List<Contact> contacts) {
        STGroupFile st = new STGroupFile("string.template.stg");
        st.registerRenderer(String.class, new StringRenderer());
        ST tpl = st.getInstanceOf("emailWithBirthdays");
        tpl.add("contacts", contacts);
        return tpl.render();
    }
}
