package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.SessionUtil;
import exceptions.ExecutingCommandsException;

public class Logout implements Command {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		new SessionUtil().closeSession(request);
		return "login";
	}
	
	@Override
	public boolean isFreeAccess() {
		return true;
	}

}
