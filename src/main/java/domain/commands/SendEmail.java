package domain.commands;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ContactDao;
import dao.impl.MysqlContactDao;
import domain.factories.StringTemplateFactory;
import domain.models.Contact;
import exceptions.AccessException;
import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import utils.EmailUtils;
import utils.ParserRequest;
import utils.RequestUtils;

import java.util.List;

public class SendEmail implements Command {

    private static Logger logger = Logger.getLogger(SendEmail.class.getName());

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {

        String message = (String) request.getAttribute("message");
        int[] idRecipients = ParserRequest.getIds(request, "idRecipients", ",");
        int amount = idRecipients.length;
        if (amount != 0) {
            List<Contact> contacts = getContacts(idRecipients);
            sendMessages(request, message, contacts);
        }
        RequestUtils.addInfoMessage(request, "Отправлено: " + amount);

        GetContacts getContacts = new GetContacts();
        return getContacts.execute(request, response);
    }

    private void sendMessages(HttpServletRequest request, String message, List<Contact> contacts) {
        contacts.stream().filter(contact -> StringUtils.isNotEmpty(contact.getEmail())).forEach(contact -> {
            try {
                String fullMessage = (new StringTemplateFactory()).fillMessage(contact, message);
                EmailUtils emailUtils = new EmailUtils();
                emailUtils.sendMessage(contact.getEmail(), (String) request.getAttribute("subject"), fullMessage);
            } catch (MessagingException e) {
                String parameter = message + " " + contact.toString();
                logger.error(parameter, e);
                throw new IncorrectDataException("Ошибка отправки email.");
            }
        });
    }

    private List<Contact> getContacts(int[] idRecipients) {
        ContactDao contactDao = new MysqlContactDao();
        return contactDao.getContactsByIdContacts(idRecipients);
    }

}
