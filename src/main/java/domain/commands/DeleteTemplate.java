package domain.commands;

import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.RequestUtils;
import utils.SessionUtil;
import utils.UserTemplateService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteTemplate implements Command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        String nameTemplate = getNameTemplate(request);
        UserTemplateService templateService = new UserTemplateService((new SessionUtil()).getUser(request).getIdUser());
        templateService.deleteTemplate(nameTemplate);
        RequestUtils.addInfoMessage(request, "Шаблон " + nameTemplate +" удален.");
        ToEmail toEmail = new ToEmail();
        return toEmail.execute(request, response);
    }

    private String getNameTemplate(HttpServletRequest request) {
        String nameTemplate = (String) request.getAttribute("template");
        if (StringUtils.isEmpty(nameTemplate) || "Шаблоны".equals(nameTemplate)) {
            throw new IncorrectDataException("Выберите шаблон.");
        }
        return nameTemplate;
    }

}
