package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.models.Navigation;
import org.apache.commons.lang3.StringUtils;
import utils.SessionUtil;
import domain.factories.ContactsDtoFactory;
import domain.models.ContactsDto;

public class GetContacts implements Command {


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
        setNavigationParameters(request);
        ContactsDtoFactory contactsDtoFactory = new ContactsDtoFactory();
		ContactsDto contactsDto = contactsDtoFactory.getContactsDto(request);
		request.setAttribute("contactsDto", contactsDto);
		
		return "main";
	}

    private void setNavigationParameters(HttpServletRequest request){
        SessionUtil sessionUtil = new SessionUtil();
        Navigation navigation = sessionUtil.getNavigation(request);
        setLimitForPage(request, navigation);
        setNumberPage(request, navigation);
    }

    private void setNumberPage(HttpServletRequest request, Navigation navigation) {
        String tmpParam = (String) request.getAttribute("numberPage");
        if (StringUtils.isNotEmpty(tmpParam)) {
            navigation.setNumberPage(Integer.parseInt(tmpParam));
        }
    }

    private void setLimitForPage(HttpServletRequest request, Navigation navigation) {
        String tmpParam = (String) request.getAttribute("limitForPage");
        if (StringUtils.isNotEmpty(tmpParam)) {
            navigation.setLimitForPage(Integer.parseInt(tmpParam));
        }
    }

}
