package domain.commands;

import dao.AddressDao;
import dao.AttachmentDao;
import dao.ContactDao;
import dao.PhoneDao;
import dao.impl.MysqlAddressDao;
import dao.impl.MysqlAttachmentDao;
import dao.impl.MysqlContactDao;
import dao.impl.MysqlPhoneDao;
import domain.models.Address;
import domain.models.Attachment;
import domain.models.Contact;
import domain.models.Phone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ToEditContact implements Command {

    private ContactDao contactDao = new MysqlContactDao();
    private AttachmentDao attachmentDao = new MysqlAttachmentDao();
    private PhoneDao phoneDao = new MysqlPhoneDao();
    private AddressDao addressDao = new MysqlAddressDao();

    private int idContact;

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        checkIdContact(request);
        Contact contact = contactDao.getById(idContact);
        Address address = addressDao.getByIdContact(idContact);
        List<Attachment> attachments = attachmentDao.getAllByIdContact(idContact);
        List<Phone> phones = phoneDao.getAllByIdContact(idContact);
        request.setAttribute("contact", contact);
        request.setAttribute("address", address);
        request.setAttribute("attachments", attachments);
        request.setAttribute("attachmentsSize", attachments.size());
        request.setAttribute("phones", phones);
        request.setAttribute("phonesSize", phones.size());
        return "edit";
    }

    private void checkIdContact(HttpServletRequest request) {
        try {
            idContact = Integer.parseInt((String) request.getAttribute("idContact"));
        } catch (Exception e) {
            idContact = -1;
        }
    }

}
