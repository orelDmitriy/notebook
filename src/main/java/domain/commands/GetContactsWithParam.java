package domain.commands;

import domain.models.Navigation;
import domain.models.SearchParameters;
import exceptions.AccessException;
import utils.ConverterDate;
import utils.SessionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetContactsWithParam implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){

        SearchParameters searchParameters = createFromRequest(request);
        SessionUtil sessionUtil = new SessionUtil();
        sessionUtil.setSearchParameters(request, searchParameters);
        Navigation navigation = sessionUtil.getNavigation(request);
        navigation.clear();


        GetContacts getContacts = new GetContacts();
        return getContacts.execute(request, response);
	}

    private SearchParameters createFromRequest(HttpServletRequest request) throws AccessException {
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.setName((String)request.getAttribute("name"));
        searchParameters.setSurname((String)request.getAttribute("surname"));
        searchParameters.setPatronymic((String)request.getAttribute("patronymic"));
        searchParameters.setMinAge(ConverterDate.parse((String)request.getAttribute("minAge")));
        searchParameters.setMaxAge(ConverterDate.parse((String)request.getAttribute("maxAge")));
        searchParameters.setSex((String)request.getAttribute("sex"));
        searchParameters.setNationality((String)request.getAttribute("nationality"));
        searchParameters.setMaritalStatus((String)request.getAttribute("maritalStatus"));
        searchParameters.setCountry((String)request.getAttribute("country"));
        searchParameters.setCity((String)request.getAttribute("city"));
        searchParameters.setStreet((String)request.getAttribute("street"));
        searchParameters.setHome((String)request.getAttribute("home"));
        searchParameters.setRoom((String)request.getAttribute("room"));
        searchParameters.setPostcode((String)request.getAttribute("postcode"));
        return searchParameters;
    }

}
