package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.validators.UserValidator;
import org.apache.commons.codec.digest.DigestUtils;

import utils.SessionUtil;
import dao.UserDao;
import dao.impl.MysqlUserDao;
import domain.models.User;
import exceptions.AccessException;
import exceptions.DaoException;
import exceptions.ExecutingCommandsException;

public class Login implements Command {

	protected UserDao userDao = new MysqlUserDao();
	protected String forward;
	protected String email;
	protected String password;
    protected UserValidator userValidator;

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		initFields(request);
        userValidator = new UserValidator(getUser());

		checkPasswords();
		new SessionUtil().initSession(request, userValidator.getUser());
		
		GetContacts getContacts = new GetContacts();
		return getContacts.execute(request, response);
	}

	private User getUser() {
		User user = userDao.getByEmail(email);
		if (user.getIdUser() < 1){
			throw new ExecutingCommandsException("Email не зарегистрирован.");
		}
		return user;
	}

	protected void initFields(HttpServletRequest request) {
		email = (String) request.getAttribute("email");
		password = (String) request.getAttribute("password");
	}

	protected void checkPasswords() {
		if (password.length() < 6
				|| !userValidator.getUser().getPassword().equals(DigestUtils.md5Hex(password))) {
			throw new ExecutingCommandsException("Неверный пароль.");
		}
	}
	

	@Override
	public boolean isFreeAccess() {
		return true;
	}

}
