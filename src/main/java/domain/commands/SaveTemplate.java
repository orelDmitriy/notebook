package domain.commands;

import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import utils.RequestUtils;
import utils.SessionUtil;
import utils.UserTemplateService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SaveTemplate implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        String subject = getSubject(request);
        String message = getMessage(request);
        UserTemplateService templateService = new UserTemplateService((new SessionUtil()).getUser(request).getIdUser());
        templateService.addTemplate(subject, message);

        RequestUtils.addInfoMessage(request, "Шаблон сохранен.");
        ToEmail toEmail = new ToEmail();
        return toEmail.execute(request, response);
    }

    private String getSubject(HttpServletRequest request) {
        String subject = (String) request.getAttribute("subject");
        if (StringUtils.isEmpty(subject)) {
            throw new IncorrectDataException("Укажите тему письма.");
        }
        return subject;
    }

    private String getMessage(HttpServletRequest request) {
        String message = (String) request.getAttribute("message");
        if (StringUtils.isEmpty(message)) {
            throw new IncorrectDataException("Шаблон не может быть пустой.");
        }
        return message;
    }



}
