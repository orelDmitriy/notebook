package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.AccessException;
import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;

public interface Command {
	
	/**
	 * @return nextPage name without .jsp 
	 */
	String execute(HttpServletRequest request,
			HttpServletResponse response);
	default boolean isFreeAccess(){
		return false;
	}
}
