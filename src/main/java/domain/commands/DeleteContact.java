package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ContactDao;
import dao.impl.MysqlContactDao;
import domain.models.User;
import domain.models.Navigation;
import utils.ParserRequest;
import utils.RequestUtils;
import utils.SessionUtil;

public class DeleteContact implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = (new SessionUtil().getUser(request));
        int[] idContacts = ParserRequest.getIds(request,"deleteIds",",");
        int deleted = deleteContactsFromDB(idContacts, user.getIdUser());
        updateNavigation(request, deleted);
        RequestUtils.addInfoMessage(request, "Контактов удалено: " + deleted);
        GetContacts getContacts = new GetContacts();
		return getContacts.execute(request, response);
	}

    private int deleteContactsFromDB(int[] idContacts, int idUser) {
        int deleted = 0;
        if (idContacts.length > 0) {
            ContactDao contactDao = new MysqlContactDao();
            deleted = contactDao.delete(idContacts, idUser);
        }
        return deleted;
    }

    private void updateNavigation(HttpServletRequest request, int deleted){
        SessionUtil sessionUtil = new SessionUtil();
        Navigation navigation = sessionUtil.getNavigation(request);
        navigation.setAmountContactsForUser(navigation.getAmountContactsForUser() - deleted);
    }

}
