package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.ExecutingCommandsException;

public class ToRegistration implements Command{

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		return "registration";
	}
	
	@Override
	public boolean isFreeAccess() {
		return true;
	}
}
