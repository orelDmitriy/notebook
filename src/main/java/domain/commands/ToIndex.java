package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.ExecutingCommandsException;

public class ToIndex implements Command {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		return "login";
	}

	@Override
	public boolean isFreeAccess() {
		return true;
	}
}
