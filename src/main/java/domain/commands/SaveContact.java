package domain.commands;

import dao.AddressDao;
import dao.AttachmentDao;
import dao.ContactDao;
import dao.PhoneDao;
import dao.impl.MysqlAddressDao;
import dao.impl.MysqlAttachmentDao;
import dao.impl.MysqlContactDao;
import dao.impl.MysqlPhoneDao;
import domain.models.Address;
import domain.models.Attachment;
import domain.models.Contact;
import domain.models.Phone;
import domain.validators.AttachmentValidator;
import domain.validators.ContactValidator;
import domain.validators.PhoneValidator;
import exceptions.AccessException;
import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;
import utils.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;

public class SaveContact implements Command {

    private boolean isNewContact = false;

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {
        Contact contact = createContactFromRequest(request);
        updateContactInBD(contact);
        updatePhotoFile(contact);
        Address address = createAddressFromRequest(request, contact.getIdContact());
        updateAddressInBD(address);

        List<Phone> phones = createPhonesFromRequest(request, contact.getIdContact());
        updatePhonesInBD(contact.getIdContact(), phones);

        List<Attachment> attachments = createAttachmentsFromRequest(request, contact.getIdContact());
        updateAttachmentFiles(attachments, contact.getIdContact());
        updateAttachmentInDB(contact.getIdContact(), attachments);

        ContactFilesService.clearTmpFiles(contact.getIdContact());

        RequestUtils.addInfoMessage(request, "Контакт сохранен.");
        GetContacts getContacts = new GetContacts();
        return getContacts.execute(request, response);
    }

    private void updateAddressInBD(Address address) {
        AddressDao addressDao = new MysqlAddressDao();
        if (addressDao.update(address) == 0) {
            addressDao.add(address);
        }
    }

    private Address createAddressFromRequest(HttpServletRequest request, int idContact) {
        Address address = new Address();
        address.setIdContact(idContact);
        address.setCountry((String) request.getAttribute("country"));
        address.setCity((String) request.getAttribute("city"));
        address.setStreet((String) request.getAttribute("street"));
        address.setHome((String) request.getAttribute("home"));
        address.setRoom((String) request.getAttribute("room"));
        address.setPostcode((String) request.getAttribute("postcode"));
        return address;
    }

    private void updatePhonesInBD(int idContact, List<Phone> phones) {
        PhoneDao phoneDao = new MysqlPhoneDao();
        if (phones.size() > 0) {
            phoneDao.updateAll(phones);
        } else {
            phoneDao.deleteAllByIdContact(idContact);
        }
    }

    private List<Phone> createPhonesFromRequest(HttpServletRequest request, int idContact) {
        List<Phone> phones = new LinkedList<>();
        String[] phonesIds = ((String) request.getAttribute("phonesIds")).split(",");
        PhoneValidator phoneValidator;
        for (int i = 1; i < phonesIds.length; i++) {
            String idPhone = phonesIds[i];

            phoneValidator = new PhoneValidator();
            phoneValidator.setFullNumber((String) request.getAttribute(idPhone.concat("number")));
            phoneValidator.setType((String) request.getAttribute(idPhone.concat("type")));
            phoneValidator.setComment((String) request.getAttribute(idPhone.concat("comment")));
            phoneValidator.setIdContact(idContact);
            phones.add(phoneValidator.getPhone());
        }
        return phones;
    }

    private void updateAttachmentInDB(int idContact, List<Attachment> attachments) {
        AttachmentDao attachmentDao = new MysqlAttachmentDao();
        if (attachments.size() > 0) {
            attachmentDao.updateAll(attachments);
        } else {
            attachmentDao.deleteAllByIdContact(idContact);
        }
    }

    private void updateAttachmentFiles(List<Attachment> attachments, int idContact) {
        String attachmentDirectory = ContactFilesService.getPathToAttachmentDirectory(idContact);
        FileUtil.createOrClearDirectory(attachmentDirectory);
        for (Attachment attachment : attachments) {
            String localPath = ContactFilesService.getLocalAttachmentPath(idContact, attachment.getFileName());
            String tmpPath = ContactFilesService.getTmpFilePath(idContact, attachment.getFileName());
            if (isNewContact) {
                tmpPath = ContactFilesService.getTmpFilePath(-1, attachment.getFileName());
            }
            FileUtil.copy(tmpPath, localPath);
        }
    }

    private List<Attachment> createAttachmentsFromRequest(
            HttpServletRequest request, int idContact) {
        List<Attachment> attachments = new LinkedList<>();
        List<Attachment> oldAttachments = getOldAttachments(idContact);
        String[] attachmentsIds = ((String) request.getAttribute("attachmentsIds")).split(",");
        String nameInRepository;
        String date;
        String comment;
        for (int i = 1; i < attachmentsIds.length; i++) {
            String idAttechment = attachmentsIds[i];
            nameInRepository = getAttechmentFileName(request, idContact,
                    oldAttachments, idAttechment);
            date = (String) request.getAttribute(idAttechment.concat("date"));
            comment = (String) request.getAttribute(idAttechment.concat("comment"));

            AttachmentValidator attachmentValidator = new AttachmentValidator();

            attachmentValidator.setFileName(nameInRepository);
            attachmentValidator.setLoadDate(ConverterDate.parse(date));
            attachmentValidator.setComment(comment);
            attachmentValidator.setIdContact(idContact);
            attachments.add(attachmentValidator.getAttachment());
        }
        return attachments;
    }

    private String getAttechmentFileName(HttpServletRequest request,
                                         int idContact, List<Attachment> oldAttachments, String idAttachment) {
        String nameInRepository;
        nameInRepository = (String) request.getAttribute(idAttachment.concat("input"));
        if (nameInRepository == null) {
            nameInRepository = checkOldAttachmentInBd(oldAttachments, idAttachment);
            copyLocalFileInTmpFolder(idContact, nameInRepository);
        }
        return nameInRepository;
    }

    private void copyLocalFileInTmpFolder(int idContact, String nameInRepository) {
        String localPath = ContactFilesService.getLocalAttachmentPath(idContact, nameInRepository);
        String tmpPath = ContactFilesService.getTmpFilePath(idContact, nameInRepository);
        FileUtil.copy(localPath, tmpPath);
    }

    private List<Attachment> getOldAttachments(int idContact) {
        List<Attachment> oldAttachments = null;
        AttachmentDao attachmentDao = new MysqlAttachmentDao();
        oldAttachments = attachmentDao.getAllByIdContact(idContact);
        return oldAttachments;
    }

    private String checkOldAttachmentInBd(List<Attachment> oldAttachments,
                                          String idAttachment) {
        for (Attachment attachment : oldAttachments) {
            if (attachment.getIdAttachment() == Integer.parseInt(idAttachment)) {
                return attachment.getFileName();
            }
        }
        return null;
    }

    private void updatePhotoFile(Contact contact) {
        if (contact.isExist() && contact.hasUpdatePhoto()) {
            String photoDirectory = ContactFilesService.getPathToPhotoDirectory(contact.getIdContact());
            FileUtil.createOrClearDirectory(photoDirectory);
            String tmpPathPhoto = ContactFilesService.getTmpFilePath(contact.getIdContact(), contact.getNamePhoto());
            if (isNewContact) {
                tmpPathPhoto = ContactFilesService.getTmpFilePath(-1, contact.getNamePhoto());
            }
            String pathPhoto = ContactFilesService.getLocalPhotoPath(contact.getIdContact(), contact.getNamePhoto());
            FileUtil.copy(tmpPathPhoto, pathPhoto);
        }
    }


    private void updateContactInBD(Contact contact) {
        ContactDao contactDao = new MysqlContactDao();
        if (contact.isExist()) {
            contactDao.update(contact);
        } else {
            int idContact = contactDao.add(contact);
            contact.setIdContact(idContact);
            isNewContact = true;
        }
    }

    private Contact createContactFromRequest(HttpServletRequest request) {
        ContactValidator contactValidator = new ContactValidator();

        contactValidator.setIdContact((String) request.getAttribute("idContact"));
        contactValidator.setIdUser((new SessionUtil()).getUser(request).getIdUser());
        contactValidator.setName((String) request.getAttribute("name"));
        contactValidator.setSurname((String) request.getAttribute("surname"));
        contactValidator.setPatronymic((String) request.getAttribute("patronymic"));
        contactValidator.setBirthday(ConverterDate.parse((String) request
                .getAttribute("birthday")));
        contactValidator.setSex((String) request.getAttribute("sex"));
        contactValidator.setNationality((String) request.getAttribute("nationality"));
        contactValidator.setMaritalStatus((String) request
                .getAttribute("maritalStatus"));
        contactValidator.setWebSite((String) request.getAttribute("webSite"));
        contactValidator.setEmail((String) request.getAttribute("email"));
        contactValidator.setPlaceOfWork((String) request.getAttribute("placeOfWork"));
        contactValidator.setNamePhoto((String) request.getAttribute("photo"));

        return contactValidator.getContact();
    }

}
