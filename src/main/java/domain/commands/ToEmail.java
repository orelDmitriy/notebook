package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ContactDao;
import dao.impl.MysqlContactDao;
import exceptions.AccessException;
import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;
import org.apache.log4j.Logger;
import utils.ParserRequest;
import utils.SessionUtil;
import utils.UserTemplateService;

import java.util.HashMap;

public class ToEmail implements Command{

    private static Logger logger = Logger.getLogger(ToEmail.class.getName());

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
        String idsParameter = (String) request.getAttribute("idRecipients");
        String emails = getEmails(request);
        HashMap<String, String> templates = getAllTemplates(request);

        request.setAttribute("emails", emails);
        request.setAttribute("idRecipients", idsParameter);
        request.setAttribute("templates", templates);
		return "email";
	}

    private HashMap<String, String> getAllTemplates(HttpServletRequest request) {
        UserTemplateService templateService = new UserTemplateService((new SessionUtil()).getUser(request).getIdUser());
        return templateService.getAllTemplates();
    }

    private String getEmails(HttpServletRequest request){
        try {
            int[] idRecipients = ParserRequest.getIds(request, "idRecipients", ",");
            ContactDao contactDao = new MysqlContactDao();
            return contactDao.getEmailsByIdContacts(idRecipients);
        } catch (Exception e){
            logger.error("Выберите хотябы 1 контакт.", e);
            throw new IncorrectDataException("Выберите хотябы 1 контакт.");
        }
    }


}
