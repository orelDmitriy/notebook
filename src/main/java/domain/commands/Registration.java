package domain.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.validators.UserValidator;
import org.apache.commons.codec.digest.DigestUtils;

import utils.SessionUtil;
import domain.models.User;
import exceptions.AccessException;
import exceptions.DaoException;
import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;

public class Registration extends Login implements Command {

	protected String repeat;

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		initFields(request);
		checkPasswords();
		createUser();
        userValidator.setIdUser(userDao.add(userValidator.getUser()));
		new SessionUtil().initSession(request, userValidator.getUser());

		GetContacts getContacts = new GetContacts();
		return getContacts.execute(request, response);
	}

	@Override
	protected void initFields(HttpServletRequest request) {
		email = (String) request.getAttribute("email");
		password = (String) request.getAttribute("password");
		repeat = (String) request.getAttribute("repeat");
	}

	@Override
	protected void checkPasswords(){
		if (password.length() < 6) {
			throw new ExecutingCommandsException("Короткий пароль.");
		}
		if (!password.equals(repeat)) {
			throw new ExecutingCommandsException("Пароли не совпадают.");
		}
	}

	protected void createUser() throws ExecutingCommandsException {
        userValidator = new UserValidator();
        userValidator.setEmail(email);
        userValidator.setPassword(DigestUtils.md5Hex(password));
	}
}
