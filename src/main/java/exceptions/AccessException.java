package exceptions;

public class AccessException extends Error {

	private static final long serialVersionUID = 993938898346970180L;

	public AccessException() {
		super();
	}

	public AccessException(String message, Exception e) {
		super(message, e);
	}

	public AccessException(String message) {
		super(message);
	}
}
