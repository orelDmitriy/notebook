package utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import domain.models.User;
import domain.models.Navigation;
import domain.models.SearchParameters;
import exceptions.AccessException;

public class SessionUtil {
	
	public void initSession(HttpServletRequest request, User user){
		if (!isClosed(request)){
			closeSession(request);
		}
		HttpSession session = request.getSession(true);
		session.setAttribute("user", user);
        session.setAttribute("navigation", new Navigation());
        session.setAttribute("searchParameters", new SearchParameters());
    }

	public User getUser(HttpServletRequest request) throws AccessException {
		if (!isClosed(request)){
			HttpSession session = request.getSession(true);
			return (User) session.getAttribute("user");
		}
		throw new AccessException();
	}

    public Navigation getNavigation(HttpServletRequest request) throws AccessException {
        if (!isClosed(request)){
            HttpSession session = request.getSession(true);
            return (Navigation) session.getAttribute("navigation");
        }
        throw new AccessException();
    }

    public SearchParameters getSearchParameters(HttpServletRequest request) throws AccessException {
        if (!isClosed(request)){
            HttpSession session = request.getSession(true);
            return (SearchParameters) session.getAttribute("searchParameters");
        }
        throw new AccessException();
    }

    public void setSearchParameters(HttpServletRequest request, SearchParameters searchParameters) {
        if (!isClosed(request)) {
            HttpSession session = request.getSession(true);
            session.setAttribute("searchParameters", searchParameters);
        }
    }

    public void closeSession(HttpServletRequest request){
        HttpSession session = request.getSession(true);
        session.setAttribute("user", null);
    }

    public boolean isClosed(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        return session.getAttribute("user") == null;
    }

}
