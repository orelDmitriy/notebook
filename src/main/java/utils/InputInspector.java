package utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputInspector {

	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = "^[a-z\\d]+[a-z\\d\\u002E\\u005F]*[a-z\\d]+@([a-z]){2,10}\\u002E[a-z]{2,4}$";
	private static final String NAME_PATTERN = "^[a-zа-яё]{1,20}$";
	private static final String WEB_SITE_PATTERN = "(http://|https://)?(www\\u002E)?([a-z0-9]+)(\\u002E[a-z0-9]+)*\\u002E[a-z]{2,4}";
	private static final String POST_CODE_PATTERN = "^[0-9a-z]{5,8}$";
	
	
	public boolean isEmail(String email) {
		return checkString(email, EMAIL_PATTERN) && email.length() <= 45;
	}

	public boolean isPassword(String password) {
		if (password == null) {
			return false;
		}
		return password.length() == 32;
	}

	public boolean isSimpleName(String name) {
		return checkString(name, NAME_PATTERN);
	}

	public boolean isId(int id) {
		return id > 0;
	}

	public boolean isUrl(String webSite) {
		return checkString(webSite, WEB_SITE_PATTERN) && webSite.length() <= 100;
	}

	public boolean isPostcode(String postcode) {
		return checkString(postcode, POST_CODE_PATTERN);
	}

    private boolean checkString(String str, String pattern) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        this.pattern = getPattern(pattern);
        matcher = this.pattern.matcher(str.toLowerCase());
        return matcher.matches();
    }

    private Pattern getPattern(String pattern) {
        return Pattern.compile(pattern);
    }

}
