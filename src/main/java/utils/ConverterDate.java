package utils;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import exceptions.DaoException;
import org.apache.commons.lang3.StringUtils;

import exceptions.IncorrectDataException;
import org.apache.log4j.Logger;

public class ConverterDate {
	private static final String PATTERN = "dd.MM.yyyy";
	private static final String PATTERN_FROM_UI = "yyyy-MM-dd";
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter
			.ofPattern(PATTERN);

    private static Logger logger = Logger.getLogger(ConverterDate.class.getName());

	public static String format(LocalDate date) {
		if (date == null) {
			return null;
		}
		return DATE_FORMATTER.format(date);
	}

	public static synchronized LocalDate parse(String dateString) {
		if (StringUtils.isNotEmpty(dateString)) {
			try {
				return DATE_FORMATTER.parse(dateString, LocalDate::from);
			} catch (DateTimeParseException e) {
				try {
					return DateTimeFormatter.ofPattern(PATTERN_FROM_UI).parse(
							dateString, LocalDate::from);
				} catch (DateTimeParseException e1) {
                    logger.error(dateString, e);
                    throw new IncorrectDataException("Ошибка конвертирования даты.");
				}
			}
		}
		return null;
	}

	public static LocalDate toLocalDate(Date date) {
		LocalDate localDate = null;
		if (date != null) {
			localDate = date.toLocalDate();
		}
		return localDate;
	}

	public static Date toDate(LocalDate localDate){
		Date date = null;
		if (localDate != null) {
			date = Date.valueOf(localDate);
		}
		return date;
	}

}
