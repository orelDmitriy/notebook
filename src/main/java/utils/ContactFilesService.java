package utils;

import java.io.File;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

public class ContactFilesService {

	private static final int LENGTH_HASH = 32;
	private static final String PHOTO_FOLDER = "photo";
	private static final String ATTACHMENTS_FOLDER = "attachment";

	public ContactFilesService(String idContact) {
		FileUtil.createDirectory(idContact);
		FileUtil.createDirectory(idContact + File.separator + PHOTO_FOLDER);
		FileUtil.createDirectory(idContact + File.separator + ATTACHMENTS_FOLDER);
	}

	public File createUniqueFile(String idContact, String name) {
		String hashName = DigestUtils.md5Hex(name);
		String prefix = FileUtil.createUniqueNameInFolder(idContact);
		String localPath = idContact + File.separator + prefix + hashName;
		if (FileUtil.exists(localPath)) {
			return createUniqueFile(idContact, name);
		}
		return new File(FileUtil.getFullPath(localPath));
	}
	
	public static String getPrefix(File file) {
		return getPrefix(file.getName());
	}
	
	public static String getPrefix(String fileName) {
		return fileName.substring(0, LENGTH_HASH);
	}

	public static String convertToNameInFolder(String name) {
		if (StringUtils.isNotEmpty(name)) {
			String prefix = name.substring(0, LENGTH_HASH);
			String hashName = DigestUtils.md5Hex(name.substring(LENGTH_HASH));
			return prefix + hashName;
		}
		return null;
	}

	public static String getTmpFilePath(int idContact, String namePhoto) {
		return getTmpFile(Integer.toString(idContact), namePhoto);
	}

	public static String getTmpFile(String idContact, String namePhoto) {
		return idContact + File.separator + convertToNameInFolder(namePhoto) + File.separator;
	}

	public static String getLocalAttachmentPath(int idContact, String namePhoto) {
		return getPathToAttachmentDirectory(idContact) + convertToNameInFolder(namePhoto);
	}
	
	public static String getLocalPhotoPath(int idContact, String namePhoto) {
		return getPathToPhotoDirectory(idContact) + convertToNameInFolder(namePhoto);
	}
	
	public static String getPathToPhotoDirectory(int idContact){
		return idContact + File.separator + PHOTO_FOLDER + File.separator;
	}

	public static void clearTmpFiles(int idContact) {
		FileUtil.clearDirectory(Integer.toString(idContact));
        FileUtil.clearDirectory(Integer.toString(-1));
    }

	public static String getPathToAttachmentDirectory(int idContact) {
		return idContact + File.separator + ATTACHMENTS_FOLDER + File.separator;
	}

	public static String getSimpleName(String fileName) {
		return fileName.replaceFirst(getPrefix(fileName), "");
	}
}
