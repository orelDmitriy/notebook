package utils;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

public class RequestUtils {

    public static void addInfoMessage(HttpServletRequest request, String message) {
        List<String> infoMessages = getInfoMessages(request);
        infoMessages.add(message);
    }

    public static void addErrorMessage(HttpServletRequest request, String message){
        List<String> errors = getErrorMessages(request);
        errors.add(message);
    }

    private static List<String> getErrorMessages(HttpServletRequest request){
        List<String> errors = (List<String>) request.getAttribute("errors");
        if(errors == null){
            errors = new LinkedList<>();
            request.setAttribute("errors", errors);
        }
        return errors;
    }

    private static List<String> getInfoMessages(HttpServletRequest request){
        List<String> infoMessages = (List<String>) request.getAttribute("infoMessages");
        if (infoMessages == null){
            infoMessages = new LinkedList<>();
            request.setAttribute("infoMessages", infoMessages);
        }
        return infoMessages;
    }
}
