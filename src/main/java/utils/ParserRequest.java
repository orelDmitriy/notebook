package utils;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

public class ParserRequest {

    public static int[] getIds(HttpServletRequest request, String nameAttribute, String separator){
        String idRecipients = (String) request.getAttribute(nameAttribute);
        return Arrays.stream(idRecipients.split(separator)).filter(id -> StringUtils.isNotEmpty(id))
                .mapToInt(Integer::parseInt).filter(id -> id > 0).toArray();
    }
}
