package utils;

import exceptions.ExecutingCommandsException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

public class FileUtil {

    private static Logger logger = Logger.getLogger(FileUtil.class.getName());

	private static String REAL_PATH;
	private static String UPLOAD_FOLDER;
    private static String LOGGERS_FOLDER;

    public static void init(String realPath){
        REAL_PATH = realPath;
        UPLOAD_FOLDER = ResourceBundle.getBundle("resources").getString(
                "repository.folder");
        LOGGERS_FOLDER = ResourceBundle.getBundle("resources").getString(
                "logs.folder");
        createDirectory("");
    }

	public static void createDirectory(String localPath) {
		File createDirectory = new File(getFullPath(localPath));
		if (!createDirectory.exists()) {
            createDirectory.mkdir();
        }
        File logsFolder = new File(REAL_PATH + File.separator + LOGGERS_FOLDER + File.separator);
        if (!logsFolder.exists()) {
            logsFolder.mkdir();
        }
	}

	public synchronized static void deleteFolder(File file) {
		if (file == null || !file.exists())
			return;
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				deleteFolder(f);
			}
			deleteSimpleFile(file);
		} else {
			deleteSimpleFile(file);
		}
	}

	public static void deleteSimpleFile(File file) {
		try {
			file.delete();
		} catch (Exception e) {
            logger.error("Файл занят другим процессом", e);
		}
	}

	public synchronized static File createTmpFolder() {
		String nameFolder = createUniqueNameInFolder("");
		File file = new File(getFullPath(nameFolder));
		file.mkdir();
		return file;
	}

	public static String createUniqueNameInFolder(String folder) {
		String name = DigestUtils.md5Hex(Long.toString(System
				.currentTimeMillis()));
		if (exists(folder + File.separator + name)) {
			return createUniqueNameInFolder(folder);
		}
		return name;
	}

	public static boolean exists(String localPath) {
		File file = new File(getFullPath(localPath));
		return file.exists();
	}

	public static String getFullPath(String localPath) {
		return REAL_PATH + File.separator + UPLOAD_FOLDER + File.separator + localPath;
	}
	
	public static String getPathInServer(String localPath){
		return UPLOAD_FOLDER + File.separator + localPath;
	}

	public static void clearDirectory(String localPath) {
		File directory = new File(getFullPath(localPath));
		if (directory.exists()) {
			for (File f : directory.listFiles()) {
				if (f.isFile()) {
					deleteSimpleFile(f);
				}
			}
		}
	}

	public static void createOrClearDirectory(String localPathDirectory) {
		File directory = new File(getFullPath(localPathDirectory));
        if (directory.exists()){
			clearDirectory(localPathDirectory);
		} else {
			directory.mkdir();
		}
	}

	public static void copy(String from, String to) {
		try {
			FileUtils.copyFile(new File(getFullPath(from)), new File(getFullPath(to)));
		} catch (IOException e) {
            logger.error("Ошибка копирования файла.", e);
            throw new ExecutingCommandsException("Ошибка копирования файла.");
        }
	}

    public static File getFile(String localPath){
        return new File(getFullPath(localPath));
    }
}
