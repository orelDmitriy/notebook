package utils;

import dao.ContactDao;
import dao.impl.MysqlContactDao;
import domain.factories.StringTemplateFactory;
import domain.models.Contact;
import exceptions.ExecutingCommandsException;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.mail.MessagingException;
import java.util.List;

public class SenderBirthdays implements Job {
    private Logger logger = Logger.getLogger(SenderBirthdays.class.getName());

    @Override
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        try {
            logger.info("began send birthdays to admin");

            String message = createMessage();
            String subject = "Именинники";

            EmailUtils emailUtils = new EmailUtils();
            emailUtils.sendMessageToAdmin(subject, message);
            logger.info("birthdays sent");
        } catch (MessagingException e) {
            logger.error("Без параметров", e);
            throw new ExecutingCommandsException("Ошибка отправки email с днями рождения");
        }
    }

    private String createMessage() {
        ContactDao dao = new MysqlContactDao();
        List<Contact> contacts = dao.getAllContactsWhoseBirthdayToday();
        StringTemplateFactory stringTemplateFactory = new StringTemplateFactory();
        String message;
        if (contacts.size() > 0) {
            message = stringTemplateFactory.createMessageWithBirthdays(contacts);
        } else {
            message = "Именинников сегодня нет.";
        }
        return message;
    }

}
