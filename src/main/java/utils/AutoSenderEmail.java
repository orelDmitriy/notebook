package utils;

import exceptions.ExecutingCommandsException;
import org.apache.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.CronScheduleBuilder.cronSchedule;

public class AutoSenderEmail {
    private Logger logger = Logger.getLogger(AutoSenderEmail.class.getName());

    public void start(){
        logger.info("Creating auto sender.");
        JobDetail job = JobBuilder.newJob(SenderBirthdays.class)
                .withIdentity("sendBirthdays", "group1").build();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("trigger3", "group1")
                .withSchedule(cronSchedule("0 0 6 * * ?"))
                .forJob(job)
                .build();
        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            logger.info("Starting auto sender.");
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            throw new ExecutingCommandsException("Ошибка запуска планировщика.");
        }

    }
}
