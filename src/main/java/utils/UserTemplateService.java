package utils;

import exceptions.ExecutingCommandsException;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;

public class UserTemplateService {

    private Logger logger = Logger.getLogger(UserTemplateService.class.getName());

    private final String USED_DIRECTORY_PREFIX = "template";
    private final String TEMPLATE_FILE_NAME = "template";
    private final String END_TEMPLATE_TAG = "[endTemplate]";
    private int idUser;

    public UserTemplateService(int idUser) {
        this.idUser = idUser;
        createUserDirectory();
        createTemplateFile();
    }

    public void addTemplate(String name, String template) {
        if (templatesContainsKey(name)){
            deleteTemplate(name);
        }
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(getTemplateFile(), true), "UTF-8"))) {
            template.replaceAll(END_TEMPLATE_TAG, "");
            writer.write(name + "\n");
            writer.write(template);
            writer.write("\n[endTemplate]\n");
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            String parameters = "name: " + name + " template: " + template;
            logger.error(parameters, e);
            throw new ExecutingCommandsException("Ошибка добавления шаблона.");
        }
    }

    public HashMap<String, String> getAllTemplates() {
        HashMap<String, String> templates = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(getTemplateFile()))) {
            String str;
            while ((str = reader.readLine()) != null) {
                if (StringUtils.isNotEmpty(str)) {
                    String name = str;
                    String template = readTemplate(reader);
                    templates.put(name, template);
                }
            }
        }  catch (IOException e) {
            logger.error("Без параметров", e);
            throw new ExecutingCommandsException("Ошибка получения шаблонов.");
        }
        return templates;
    }

    public void deleteTemplate(String nameTemplate) {
        try (BufferedReader reader = new BufferedReader(new FileReader(getTemplateFile()))) {
            String str;
            String anotherTemplate = "";
            while ((str = reader.readLine()) != null) {
                if (str.equals(nameTemplate)) {
                    skipDeletingTemplate(reader);
                } else {
                    anotherTemplate += str + "\n";
                }
            }
            rewriteFile(anotherTemplate);
        }  catch (IOException e) {
            logger.error(nameTemplate, e);
            throw new ExecutingCommandsException("Ошибка удалнения шаблона.");
        }
    }

    private void createUserDirectory() {
        FileUtil.createDirectory(getUserDirectoryPath());
    }

    private void createTemplateFile() {
        try {
            File templateFile = getTemplateFile();
            if (!templateFile.exists()) {
                templateFile.createNewFile();
            }
        } catch (IOException e) {
            logger.error("Без параметров", e);
            throw new ExecutingCommandsException("Ошибка создания файла с шаблонами");
        }

    }

    private boolean templatesContainsKey(String name) {
        HashMap<String, String> allTemplates = getAllTemplates();
        return allTemplates.containsKey(name);
    }

    private File getTemplateFile() {
        return FileUtil.getFile(getUserDirectoryPath() + File.separator + TEMPLATE_FILE_NAME);
    }

    private String readTemplate(BufferedReader reader) throws IOException {
        String template = "";
        String str;
        while (!(str = reader.readLine()).equals(END_TEMPLATE_TAG)) {
            template += str + "\n";
        }
        return template;
    }

    private void rewriteFile(String anotherTemplate) throws IOException {
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(getTemplateFile(), false), "UTF-8"))) {
            writer.write(anotherTemplate);
        }
    }

    private void skipDeletingTemplate(BufferedReader reader) throws IOException {
        while (!reader.readLine().equals(END_TEMPLATE_TAG));
    }


    private String getUserDirectoryPath() {
        return USED_DIRECTORY_PREFIX + idUser;
    }
}
