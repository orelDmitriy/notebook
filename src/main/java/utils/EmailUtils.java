package utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.ResourceBundle;

public class EmailUtils {

    private static Properties properties;
    private static ResourceBundle resource;

    static {
        properties = new Properties();
        resource = ResourceBundle.getBundle("email");
        properties.put("mail.smtp.host", resource.getString("mail.smtp.host"));
        properties.put("mail.smtp.port", resource.getString("mail.smtp.port"));
        properties.put("mail.smtp.auth", resource.getString("mail.smtp.auth"));
        properties.put("mail.smtp.ssl.trust", resource.getString("mail.smtp.ssl.trust"));
        properties.put("mail.smtp.starttls.enable",resource.getString("mail.smtp.starttls.enable"));
        properties.put("mail.debug",resource.getString("mail.debug"));
    }

    public void sendMessage(String emailRecipient,String subject, String textMessage) throws MessagingException {
        Session session = Session.getDefaultInstance(properties, new Authenticator(){
           @Override
            protected PasswordAuthentication getPasswordAuthentication(){
               return new PasswordAuthentication(resource.getString("sender.email"), resource.getString("sender.password"));
           }
        });
        MimeMessage message = new MimeMessage(session);
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailRecipient));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress("oreldmitriy1991@gmail.com"));
        message.setSubject(subject);
        message.setText(textMessage);

        Transport transport = session.getTransport("smtps");
        transport.send(message);
    }

    public void sendMessageToAdmin(String subject, String textMessage) throws MessagingException {
        sendMessage(resource.getString("admins.email"), subject, textMessage);
    }


}
