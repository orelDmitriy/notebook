package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import domain.validators.AttachmentValidator;
import exceptions.DaoException;
import org.apache.log4j.Logger;
import utils.ConverterDate;
import dao.AttachmentDao;
import dao.ConnectionFactory;
import domain.models.Attachment;
import exceptions.IncorrectDataException;

public class MysqlAttachmentDao implements AttachmentDao {

    private Logger logger = Logger.getLogger(MysqlAttachmentDao.class.getName());

	@Override
	public List<Attachment> getAllByIdContact(int idContact) {
		String sql = "SELECT * FROM attachment WHERE attachment.idContact = ?";
        String sentSql = null;
		List<Attachment> attachments = new LinkedList<>();
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, idContact);
                sentSql = statement.toString();
				try (ResultSet resultSet = statement.executeQuery()) {

					while (resultSet.next()) {
                        AttachmentValidator attachmentValidator = new AttachmentValidator();
                        attachmentValidator.setIdAttachment(resultSet
								.getInt("idAttachment"));
                        attachmentValidator.setIdContact(resultSet.getInt("idContact"));
                        attachmentValidator.setFileName(resultSet.getString("fileName"));
                        attachmentValidator.setLoadDate(ConverterDate
								.toLocalDate(resultSet.getDate("loadDate")));
                        attachmentValidator.setComment(resultSet.getString("comment"));
						
						attachments.add(attachmentValidator.getAttachment());
					}
				}
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка поиска присоединенных файлов.");
		}
		return attachments;
	}

	@Override
	public void addAll(List<Attachment> attachments) {
		if (attachments.size() > 0) {
			String sql = "INSERT INTO attachment VALUE(?,?,?,?,?)";
            String sentSql = null;
			try (Connection connection = ConnectionFactory.instance
					.getNewConnection()) {
				try (PreparedStatement statement = connection
						.prepareStatement(sql)) {
					for (Attachment attachment : attachments) {
						statement.setNull(1, Types.NULL);
						statement.setInt(2, attachment.getIdContact());
						statement.setString(3, attachment.getFileName());
						statement.setDate(4,
								ConverterDate.toDate(attachment.getLoadDate()));
						statement.setString(5, attachment.getComment());
                        sentSql += statement.toString() + "\n";
						statement.addBatch();
					}
					statement.executeBatch();
				}
			} catch (SQLException e) {
                logger.error(sentSql, e);
                throw new DaoException("Ошибка добавления файлов.");
			}
		}
	}

	@Override
	public void updateAll(List<Attachment> attachments) {
		if (attachments.size() > 0) {
			deleteAllByIdContact(attachments.get(0).getIdContact());
			addAll(attachments);
		}
	}

	@Override
	public void deleteAllByIdContact(int idContact) {
		String sql = "DELETE FROM attachment WHERE idContact = ?";
        String sentSql = null;
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, idContact);
                sentSql = statement.toString();
				statement.executeUpdate();
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка удаления файлов.");
		}
	}

}
