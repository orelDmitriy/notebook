package dao.impl;

import dao.ConnectionFactory;
import dao.ContactDao;
import domain.models.Contact;
import domain.models.SearchParameters;
import domain.validators.ContactValidator;
import exceptions.DaoException;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import utils.ConverterDate;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class MysqlContactDao implements ContactDao {

    private Logger logger = Logger.getLogger(MysqlContactDao.class.getName());

    @Override
    public Contact getById(int idContact) {
        String sql = "SELECT * FROM contact WHERE contact.idContact = ?";
        String sentSql = null;
        Contact contact = new Contact();
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, idContact);
                sentSql = statement.toString();
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        contact = getContactFromResultSet(resultSet);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка поиса контакта.");
        }
        return contact;
    }

    private Contact getContactFromResultSet(ResultSet resultSet)
            throws SQLException, IncorrectDataException {
        ContactValidator contactValidator = new ContactValidator();
        contactValidator.setIdContact(resultSet.getInt("idContact"));
        contactValidator.setIdUser(resultSet.getInt("idUser"));
        contactValidator.setName(resultSet.getString("name"));
        contactValidator.setSurname(resultSet.getString("Surname"));
        contactValidator.setPatronymic(resultSet.getString("patronymic"));
        contactValidator.setBirthday(ConverterDate.toLocalDate(resultSet
                .getDate("birthday")));
        contactValidator.setSex(resultSet.getString("sex"));
        contactValidator.setNationality(resultSet.getString("nationality"));
        contactValidator.setMaritalStatus(resultSet.getString("maritalStatus"));
        contactValidator.setWebSite(resultSet.getString("webSite"));
        contactValidator.setEmail(resultSet.getString("email"));
        contactValidator.setPlaceOfWork(resultSet.getString("placeOfWork"));
        contactValidator.setNamePhoto(resultSet.getString("urlPhoto"));
        return contactValidator.getContact();
    }

    @Override
    public List<Contact> getAllByIdUserWithParameters(int idUser, int numberOfPage,
                                                      int limitForPage, SearchParameters searchParameters) {
        String sql = "SELECT * " + createSearchSql(searchParameters) + " LIMIT ?,?";
        String sentSql = null;
        List<Contact> contacts = new LinkedList<>();
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, idUser);
                int numberParameter = setParameterForStatement(statement, searchParameters);
                statement.setInt(numberParameter, (numberOfPage - 1) * limitForPage);
                numberParameter++;
                statement.setInt(numberParameter, limitForPage);
                sentSql = statement.toString();
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        Contact contact = getContactFromResultSet(resultSet);
                        contacts.add(contact);
                    }
                }
            }
        } catch (SQLException e) {
            String parameters = "idUser: " + idUser + " number of page: " + numberOfPage + " limit for page: " + limitForPage;
            parameters += searchParameters.toString() + sentSql;
            logger.error(parameters, e);
            throw new DaoException("Ошибка поиска контактов по параметрам поиска.");
        }
        return contacts;
    }

    private int setParameterForStatement(PreparedStatement statement, SearchParameters searchParameters) throws SQLException {
        int numberParameter = 2;
        if (StringUtils.isNotEmpty(searchParameters.getName())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getName()));
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getSurname())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getSurname()));
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getPatronymic())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getPatronymic()));
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getSex())) {
            statement.setString(numberParameter, searchParameters.getSex());
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getNationality())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getNationality()));
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getMaritalStatus())) {
            statement.setString(numberParameter, searchParameters.getMaritalStatus());
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getCountry())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getCountry()));
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getCity())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getCity()));
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getStreet())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getStreet()));
            numberParameter++;
        }
        if (searchParameters.getHome() != 0) {
            statement.setInt(numberParameter, searchParameters.getHome());
            numberParameter++;
        }
        if (searchParameters.getRoom() > 0) {
            statement.setInt(numberParameter, searchParameters.getRoom());
            numberParameter++;
        }
        if (StringUtils.isNotEmpty(searchParameters.getPostcode())) {
            statement.setString(numberParameter, convertToRegExp(searchParameters.getPostcode()));
            numberParameter++;
        }

        if (searchParameters.getMinAge() != null) {
            statement.setDate(numberParameter, ConverterDate.toDate(searchParameters.getMinAge()));
            numberParameter++;
        }
        if (searchParameters.getMaxAge() != null) {
            statement.setDate(numberParameter, ConverterDate.toDate(searchParameters.getMaxAge()));
            numberParameter++;
        }
        return numberParameter;
    }

    private String convertToRegExp(String str) {
        return "%" + str + "%";
    }

    private String createSearchSql(SearchParameters searchParameters) {
        String sql = "FROM contact AS c JOIN address AS a ON c.idContact = a.idContact WHERE c.idUser = ? ";
        if (StringUtils.isNotEmpty(searchParameters.getName())) {
            sql += " and lower(c.name) LIKE ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getSurname())) {
            sql += " and lower(c.surname) LIKE ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getPatronymic())) {
            sql += " and lower(c.patronymic) LIKE ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getSex())) {
            sql += " and lower(c.sex) = ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getNationality())) {
            sql += " and lower(c.nationality) LIKE ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getMaritalStatus())) {
            sql += " and lower(c.maritalStatus) = ? ";
        }
        if (searchParameters.getMinAge() != null) {
            sql += " and c.birthday >= ? ";
        }
        if (searchParameters.getMaxAge() != null) {
            sql += " and c.birthday <= ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getCountry())) {
            sql += " and lower(a.country) LIKE ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getCity())) {
            sql += " and lower(a.city) LIKE ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getStreet())) {
            sql += " and lower(a.street) LIKE ? ";
        }
        if (searchParameters.getHome() > 0) {
            sql += " and lower(a.home) = ? ";
        }
        if (searchParameters.getRoom() > 0) {
            sql += " and lower(a.room)= ? ";
        }
        if (StringUtils.isNotEmpty(searchParameters.getPostcode())) {
            sql += " and lower(a.postcode) LIKE ? ";
        }
        return sql;
    }

    @Override
    public int add(Contact contact) {
        String sql = "INSERT INTO contact VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        String sentSql = null;
        int insertId = -1;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql,
                    PreparedStatement.RETURN_GENERATED_KEYS)) {
                statement.setNull(1, Types.NULL);
                statement.setInt(2, contact.getIdUser());
                statement.setString(3, contact.getName());
                statement.setString(4, contact.getSurname());
                statement.setString(5, contact.getPatronymic());
                statement.setDate(6, ConverterDate.toDate(contact.getBirthday()));
                statement.setString(7, contact.getSex().toString());
                statement.setString(8, contact.getNationality());
                statement.setString(9, contact.getMaritalStatus().toString());
                statement.setString(10, contact.getWebSite());
                statement.setString(11, contact.getEmail());
                statement.setString(12, contact.getPlaceOfWork());
                statement.setString(13, contact.getNamePhoto());
                sentSql = statement.toString();
                statement.executeUpdate();
                try (ResultSet resultSet = statement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        insertId = resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка добавления контакта.");
        }
        return insertId;
    }

    @Override
    public void update(Contact contact) {
        if (contact.getIdContact() < 1) {
            add(contact);
            return;
        }
        String sql = createSQL(contact);
        String sentSql = null;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                int lastNumber = setParameterStatement(contact, statement);
                statement.setInt(lastNumber, contact.getIdContact());
                sentSql = statement.toString();
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка обновления данных контакта.");
        }
    }

    private int setParameterStatement(Contact contact,
                                      PreparedStatement statement) throws SQLException,
            IncorrectDataException {
        statement.setString(1, contact.getName());
        statement.setString(2, contact.getSurname());
        statement.setString(3, contact.getPatronymic());
        statement.setDate(4,
                ConverterDate.toDate(contact.getBirthday()));
        statement.setString(5, contact.getSex().toString());
        statement.setString(6, contact.getNationality());
        statement.setString(7, contact.getMaritalStatus().toString());
        statement.setString(8, contact.getWebSite());
        statement.setString(9, contact.getEmail());
        statement.setString(10, contact.getPlaceOfWork());
        int numberParameter = 11;
        if (StringUtils.isNotEmpty(contact.getNamePhoto())) {
            statement.setString(numberParameter, contact.getNamePhoto());
            numberParameter++;
        }
        return numberParameter;
    }

    private String createSQL(Contact contact) {
        String sql = "UPDATE contact SET name = ?, surname = ?, patronymic = ?, birthday = ?, sex = ?,"
                + "nationality = ?, maritalStatus = ?, webSite = ?, email = ?, placeOfWork = ? ";
        if (StringUtils.isNotEmpty(contact.getNamePhoto())) {
            sql += ", urlPhoto = ?";
        }
        sql += " WHERE idContact = ?";
        return sql;
    }

    @Override
    public int delete(int[] idContacts, int idUser) {
        int deleted;
        String sql = "DELETE FROM contact WHERE idContact = ? && idUser = ?";
        String sentSql = null;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                for (int i = 0; i < idContacts.length; i++) {
                    statement.setInt(1, idContacts[i]);
                    statement.setInt(2, idUser);
                    sentSql += statement.toString() + "\n";
                    statement.addBatch();
                }
                deleted = (statement.executeBatch()).length;
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка удаления контакта.");
        }
        return deleted;
    }

    @Override
    public int getAmountContactsForUser(int idUser, SearchParameters searchParameters) {
        int amount = 0;
        String sql = "SELECT COUNT(c.idContact) AS amount " + createSearchSql(searchParameters);
        String sentSql = null;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, idUser);
                setParameterForStatement(statement, searchParameters);
                sentSql = statement.toString();
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        amount = resultSet.getInt("amount");
                    }
                }
            }
        } catch (SQLException e) {
            String parameters = "idUser: " + idUser;
            parameters += "\n" + searchParameters.toString() + "\n" + sentSql;
            logger.error(parameters, e);
            throw new DaoException("Ошибка подсчета колличества контактов пользователя.");
        }
        return amount;
    }

    @Override
    public String getEmailsByIdContacts(int[] idRecipients) {
        String emails = "";
        if (idRecipients.length == 0) {
            return emails;
        }
        String sql = "SELECT c.email AS email FROM contact AS c WHERE c.idContact = ?";
        String sentSql = null;
        sql += getSqlForIdRecipients(idRecipients);
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                setParameterForEmailsStatement(statement, idRecipients);
                sentSql = statement.toString();
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        emails += " " + resultSet.getString("email");
                    }
                    if (emails.length() > 0) {
                        emails = StringUtils.normalizeSpace(emails);
                    }
                }

            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка получения emails указанных контактов.");
        }
        return emails;
    }

    @Override
    public List<Contact> getContactsByIdContacts(int[] idContacts) {
        List<Contact> contacts = new LinkedList<>();
        if (idContacts.length == 0) {
            return contacts;
        }
        String sql = "SELECT * FROM contact WHERE contact.idContact = ?";
        String sentSql = null;
        sql += addIdContacts(idContacts);
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                setIdContactsForStatement(statement, idContacts);
                sentSql = statement.toString();
                executeQuery(contacts, statement);
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка получения данных указанных контактов.");
        }
        return contacts;
    }

    private void executeQuery(List<Contact> contacts, PreparedStatement statement) throws SQLException {
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Contact contact = getContactFromResultSet(resultSet);
                contacts.add(contact);
            }
        }
    }

    @Override
    public List<Contact> getAllContactsWhoseBirthdayToday() {
        String sql = "SELECT * FROM contact WHERE day(contact.birthday) = day(CURRENT_DATE) && month(contact.birthday) = month(CURRENT_DATE)";
        String sentSql = null;
        List<Contact> contacts = new LinkedList<>();
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                sentSql = statement.toString();
                executeQuery(contacts, statement);
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка получения списка контактов с указанным днем рождения.");
        }
        return contacts;
    }

    private void setIdContactsForStatement(PreparedStatement statement, int[] idContacts) throws SQLException {
        for (int i = 0; i < idContacts.length; i++) {
            statement.setInt(i + 1, idContacts[i]);
        }
    }

    private String addIdContacts(int[] idContacts) {
        String result = "";
        for (int i = 1; i < idContacts.length; i++) {
            result += " or contact.idContact = ?";
        }
        return result;
    }

    private void setParameterForEmailsStatement(PreparedStatement statement, int[] idRecipients) throws SQLException {
        for (int i = 0; i < idRecipients.length; i++) {
            statement.setInt(i + 1, idRecipients[i]);
        }
    }

    private String getSqlForIdRecipients(int[] idRecipients) {
        String where = "";
        for (int i = 1; i < idRecipients.length; i++) {
            where += " OR c.idContact = ?";
        }
        return where;
    }

}
