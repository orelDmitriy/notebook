package dao.impl;

import dao.AddressDao;
import dao.ConnectionFactory;
import domain.models.Address;
import exceptions.DaoException;
import exceptions.IncorrectDataException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.*;

public class MysqlAddressDao implements AddressDao {

    private Logger logger = Logger.getLogger(MysqlAddressDao.class.getName());

    @Override
    public Address getByIdContact(int idContact) {
        String sql = "SELECT * FROM address WHERE address.idContact = ? LIMIT 1";
        String sentSql = null;
        Address address = new Address();
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, idContact);
                sentSql = statement.toString();
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        address.setIdAddress(resultSet.getInt("idAddress"));
                        address.setIdContact(resultSet.getInt("idContact"));
                        address.setCountry(resultSet.getString("country"));
                        address.setCity(resultSet.getString("city"));
                        address.setStreet(resultSet.getString("street"));
                        address.setHome(resultSet.getInt("home"));
                        address.setRoom(resultSet.getInt("room"));
                        address.setPostcode(resultSet.getString("postcode"));
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка поиск адреса.");
        }
        return address;
    }

    @Override
    public int add(Address address) {
        String sql = "INSERT INTO address VALUE(?,?,?,?,?,?,?,?)";
        String sentSql = null;
        int insertId = -1;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql,
                    PreparedStatement.RETURN_GENERATED_KEYS)) {
                statement.setNull(1, Types.NULL);
                statement.setInt(2, address.getIdContact());
                statement.setString(3, address.getCountry());
                statement.setString(4, address.getCity());
                statement.setString(5, address.getStreet());
                statement.setInt(6, address.getHome());
                statement.setInt(7, address.getRoom());
                statement.setString(8, address.getPostcode());
                sentSql = statement.toString();
                statement.executeUpdate();
                try (ResultSet resultSet = statement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        insertId = resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка добавления адреса.");
        }
        return insertId;
    }

    @Override
    public void delete(Address address) {
        String sql = "DELETE FROM address WHERE idContact = ?";
        String sentSql = null;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, address.getIdContact());
                sentSql = statement.toString();
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка удаления адреса.");
        }
    }

    @Override
    public int update(Address address) {
        int amountUpdate = 0;
        String sql = "UPDATE address SET country = ?, city = ?, street = ?, home = ?, room = ?, postcode = ? WHERE idContact = ?";
        String sentSql = null;
        try (Connection connection = ConnectionFactory.instance
                .getNewConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(7, address.getIdContact());
                statement.setString(1, address.getCountry());
                statement.setString(2, address.getCity());
                statement.setString(3, address.getStreet());
                statement.setInt(4, address.getHome());
                statement.setInt(5, address.getRoom());
                statement.setString(6, address.getPostcode());
                sentSql = statement.toString();
                amountUpdate = statement.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка обновления адреса.");
        }
        return amountUpdate;
    }

}
