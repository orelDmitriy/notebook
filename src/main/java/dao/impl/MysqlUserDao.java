package dao.impl;

import dao.ConnectionFactory;
import dao.UserDao;
import domain.models.User;
import domain.validators.UserValidator;
import exceptions.DaoException;
import org.apache.log4j.Logger;

import java.sql.*;

public class MysqlUserDao implements UserDao {

    private Logger logger = Logger.getLogger(MysqlUserDao.class.getName());

	@Override
	public User getByEmail(String email)  {
		String sql = "SELECT * FROM user WHERE user.email = ?";
        String sentSql = null;
        UserValidator userValidator = new UserValidator();
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, email);
                sentSql = statement.toString();
				try (ResultSet resultSet = statement.executeQuery()) {
					if (resultSet.next()) {
                        userValidator.setIdUser(resultSet.getInt("idUser"));
                        userValidator.setEmail(resultSet.getString("email"));
                        userValidator.setPassword(resultSet.getString("password"));
					} 
				}
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Email не найден.");
		}	
		return userValidator.getUser();
	}

    @Override
	public int add(User user)  {
		String sql = "INSERT INTO user VALUE(?,?,?)";
        String sentSql = null;
		int insertId = -1;
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql,
					PreparedStatement.RETURN_GENERATED_KEYS)) {
				statement.setNull(1, Types.NULL);
				statement.setString(2, user.getEmail());
				statement.setString(3, user.getPassword());
                sentSql = statement.toString();
				statement.executeUpdate();
				try (ResultSet resultSet = statement.getGeneratedKeys()) {
					if (resultSet.next()) {
						insertId = resultSet.getInt(1);
					}
				}

			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Email занят.");
		}
		return insertId;
	}

	@Override
	public void updatePassword(User user)  {
		String sql = "UPDATE user SET password = ? WHERE user.email = ?";
        String sentSql = null;
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, user.getPassword());
				statement.setString(2, user.getEmail());
                sentSql = statement.toString();
				statement.executeUpdate();
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка обновления пароля.");
		}

	}

	@Override
	public void delete(User user)  {
		String sql = "DELETE FROM user WHERE email = ?";
        String sentSql = null;
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, user.getEmail());
                sentSql = statement.toString();
				statement.executeUpdate();
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка удаления пользователя.");
		}

	}
}
