package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import dao.ConnectionFactory;
import dao.PhoneDao;
import domain.models.Phone;
import domain.validators.PhoneValidator;
import exceptions.DaoException;
import exceptions.IncorrectDataException;
import org.apache.log4j.Logger;

public class MysqlPhoneDao implements PhoneDao {

    private Logger logger = Logger.getLogger(MysqlPhoneDao.class.getName());

	@Override
	public List<Phone> getAllByIdContact(int idContact) {
		String sql = "SELECT * FROM phone WHERE phone.idContact = ?";
        String sentSql = null;
		List<Phone> phones = new LinkedList<>();

		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, idContact);
				try (ResultSet resultSet = statement.executeQuery()) {
					PhoneValidator phoneValidator;
                    sentSql = statement.toString();
					while (resultSet.next()) {
                        phoneValidator = new PhoneValidator();
                        phoneValidator.setIdPhone(resultSet.getInt("idPhone"));
                        phoneValidator.setIdContact(resultSet.getInt("idContact"));
                        phoneValidator.setCountryCode(resultSet.getInt("countryCode"));
                        phoneValidator.setOperatorCode(resultSet.getInt("operatorCode"));
                        phoneValidator.setNumber(resultSet.getInt("number"));
                        phoneValidator.setType(resultSet.getString("type"));
                        phoneValidator.setComment(resultSet.getString("comment"));
						phones.add(phoneValidator.getPhone());
					}
				}
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка получения списка телефонов.");
		}
		return phones;
	}

	@Override
	public void addAll(List<Phone> phones) {
		if (phones.size() > 0) {
			String sql = "INSERT INTO phone VALUE(?,?,?,?,?,?,?)";
            String sentSql = null;
			try (Connection connection = ConnectionFactory.instance
					.getNewConnection()) {
				try (PreparedStatement statement = connection
						.prepareStatement(sql)) {
					for (Phone phone : phones) {
						statement.setNull(1, Types.NULL);
						statement.setInt(2, phone.getIdContact());
						statement.setInt(3, phone.getCountryCode());
						statement.setInt(4, phone.getOperatorCode());
						statement.setInt(5, phone.getNumber());
						statement.setString(6, phone.getType().toString());
						statement.setString(7, phone.getComment());
                        sentSql += statement.toString() + "\n";
						statement.addBatch();
					}
                    statement.executeBatch();
				}
			} catch (SQLException e) {
                logger.error(sentSql, e);
                throw new DaoException("Ошибка добавления телефонов.");
			}
		}
	}

	@Override
	public void updateAll(List<Phone> phones) {
		if (phones.size() > 0) {
			deleteAllByIdContact(phones.get(0).getIdContact());
			addAll(phones);
		}
	}

	@Override
	public void deleteAllByIdContact(int idContact) {
		String sql = "DELETE FROM phone WHERE idContact = ?";
        String sentSql = null;
		try (Connection connection = ConnectionFactory.instance
				.getNewConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, idContact);
                sentSql = statement.toString();
				statement.executeUpdate();
			}
		} catch (SQLException e) {
            logger.error(sentSql, e);
            throw new DaoException("Ошибка удаления телефонов");
		}
	}

}
