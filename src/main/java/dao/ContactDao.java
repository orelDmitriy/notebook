package dao;

import java.util.List;

import domain.models.Contact;
import domain.models.SearchParameters;

public interface ContactDao {
	Contact getById(int idContact);
	List<Contact> getAllByIdUserWithParameters(int idUser, int numberOfPage, int limitForPage, SearchParameters searchParameters);
	/**
	 * @return idContact
	 */
	int add(Contact contact);
	void update(Contact contact);
	int delete(int[] idContacts, int idUser);
    int getAmountContactsForUser(int idUser,  SearchParameters searchParameters);
    String getEmailsByIdContacts(int[] idRecipients);
    List<Contact> getContactsByIdContacts(int[] idRecipients);
    List<Contact> getAllContactsWhoseBirthdayToday();
}
