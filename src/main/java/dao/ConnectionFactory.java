package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import exceptions.DaoException;
import org.apache.log4j.Logger;

public class ConnectionFactory {

    private Logger logger = Logger.getLogger(ConnectionFactory.class.getName());

	public static final ConnectionFactory instance = new ConnectionFactory();

	private String url;
	private Properties properties;
	private ResourceBundle resource;
	
	private ConnectionFactory() {
		resource = ResourceBundle.getBundle("database");
		properties = new Properties();
		setProperties();
		registerDriver();
	}
	
	private void setProperties() {
		url = resource.getString("db.url");
		properties.put("user", resource.getString("db.user"));
		properties.put("password", resource.getString("db.password"));
		properties.put("autoReconnect", resource.getString("db.autoreconect"));
		properties.put("characterEncoding", resource.getString("db.encoding"));
		properties.put("setUnicode", resource.getString("db.useUnicode"));
		properties.put("autoCommit", "false");
	}

	private void registerDriver() {
		try {
			Class.forName(resource.getString("db.driver"));	
		} catch (ClassNotFoundException e) {
            logger.error("Класс драйвера не найден.", e);
            throw new DaoException("Класс драйвера не найден.", e);
        }
	}

	public Connection getNewConnection() {	
		Connection connection;
		try {
			connection = DriverManager.getConnection(url, properties);
		} catch (SQLException e) {
            logger.error("Не смог подключиться к базе данных.", e);
            throw new DaoException("Не смог подключиться к базе данных.", e);
		}
		return connection;
	}


	
}
