package dao;

import domain.models.User;
import exceptions.DaoException;

public interface UserDao {
	
	User getByEmail(String email);
	/**
	 * @return idUser
	 */
	int add(User user);
	void updatePassword(User user);
	void delete(User user);
}
