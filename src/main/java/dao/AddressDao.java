package dao;

import domain.models.Address;
import exceptions.DaoException;

public interface AddressDao {
	Address getByIdContact(int idContact);
	/**
	 * @return idAddress
	 */
	int add(Address address);
	int update(Address address);
	void delete(Address idAddress);
}
