package dao;

import java.util.List;

import domain.models.Attachment;
import exceptions.DaoException;

public interface AttachmentDao {
	List<Attachment> getAllByIdContact(int idContact);
	void addAll(List<Attachment> attachments);
	void updateAll(List<Attachment> attachments);
	void deleteAllByIdContact(int idContact);
}
