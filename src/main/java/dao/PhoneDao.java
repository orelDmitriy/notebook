package dao;

import java.util.List;

import domain.models.Phone;
import exceptions.DaoException;

public interface PhoneDao {
	
	List<Phone> getAllByIdContact(int idContact);
	void addAll(List<Phone> phones);
	void updateAll(List<Phone> phones);
	void deleteAllByIdContact(int idContact);
	
}
